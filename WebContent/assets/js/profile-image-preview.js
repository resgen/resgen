$(document).ready(function(){ 
	$('#img-preview').on('click', function () {
		$('#file-upload-button').click();
	});

	$('#file-upload-button').change(function () {
		var file = this.files[0];
		var reader = new FileReader();
		reader.onloadend = function () {
			$('#img-preview').attr('src',  reader.result );
		}
		if (file) {
			reader.readAsDataURL(file);
		} else {
		}
	});
});