$(document).ready(function() { 
	$('body').on('click', '#Deactivate', function() {  
		var $item = $(this).attr('skill-id');
		var id = $item;   

		$.ajax({
			type : 'GET',
			data : {
				id : id,
				status : "Active"
			},
			url : 'SkillMaintenance/Skill/Status', 
			success : function(res) { 
				location.reload();
			} 
		});  
	});

	$('body').on('click', '#Activate', function() {  
		var $item = $(this).attr('skill-id');
		var id = $item;  

		$.ajax({
			type : 'GET',
			data : {
				id : id,
				status : "Inactive"
			},
			url : 'SkillMaintenance/Skill/Status', 
			success : function(res) {
				location.reload();
			}
		});  
	}); 

});