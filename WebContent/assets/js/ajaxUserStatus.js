$(document).ready(function() { 
	$('body').on('click', '#Deactivate', function() {  
		var $item = $(this).attr('user-id');
		var id = $item;   

		$.ajax({
			type : 'GET',
			data : {
				id : id,
				status : "Active"
			},
			url : 'UserMaintenance/User/Status', 
			success : function(res) { 
				location.reload();
			} 
		});  
	});

	$('body').on('click', '#Activate', function() {  
		var $item = $(this).attr('user-id');
		var id = $item;  

		$.ajax({
			type : 'GET',
			data : {
				id : id,
				status : "Inactive"
			},
			url : 'UserMaintenance/User/Status', 
			success : function(res) {
				location.reload();
			}
		});  
	}); 

});