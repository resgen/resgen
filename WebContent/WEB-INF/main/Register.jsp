<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Register</title>
    <jsp:directive.include file="constant/Head.jsp" /> 
</head>
<body> 
    <div class="form-page container-fluid">
        <div class="form">
            <h1>Employee Profile</h1>
            <form action="Register" method="post" id="register"> 
                <input type="text" name="employeeId" placeholder="Employee ID" required>
				<c:if test="${RegisterDuplicateError != null}">
					<div class="alert alert-warning fade in">
						<a href="#" class="close" data-dismiss="alert"> &times;</a> <strong>Warning!</strong>
						${RegisterDuplicateError}
					</div>
				</c:if> 
                <input type="text" name="firstName" placeholder="First Name" required>
                <input type="text" name="middleName" placeholder="Middle Name" required>
                <input type="text" name="lastName" placeholder="Last Name" required>  
                <input type="email" name="email" placeholder="Email" required> 
                <c:if test="${RegisterEmailError != null}">
					<div class="alert alert-warning fade in">
						<a href="#" class="close" data-dismiss="alert"> &times;</a> <strong>Warning!</strong>
						${RegisterEmailError}
					</div>
				</c:if>
                <button type="submit" form="register" value="Submit">Register</button> 
            </form>   
            <br>
            <a href="Login">Back to login</a>            
            <!-- Error Validation --> 
			<c:if test="${RegisterNullError != null}">  
				<div class="alert alert-warning fade in">
					<a href="#" class="close" data-dismiss="alert"  > &times;</a>
					<strong>Warning!</strong> ${RegisterNullError}
				</div> 
			</c:if>  
			<!-- Success Validation -->
			<c:if test="${SuccessRegister != null}">  
				<div class="alert alert-success fade in">
					<a href="#" class="close" data-dismiss="alert"  > &times;</a>
					<strong>Success!</strong> ${SuccessRegister}
				</div> 
			</c:if>  			
		</div>
    </div>
</body>
</html>