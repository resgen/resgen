<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Experience</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
    <div class="container-fluid">
    	<div class="panel panel-primary">
	    	<div class="panel panel-heading">
	    		<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Experience Projects Edit</p></li>
				</ul>
	    	</div>
		    <div class="panel-body"> 
				<form class="form-horizontal" action="${context}/Experience/Company/Project/Edit" method="post" id="create">  
					<c:forEach items="${projectInfo}" var="project">  
						<div class="form-group">  
							<input type="hidden" class="form-control" name="projectId" value="${project.id}" hidden>
							<input type="hidden" class="form-control" name="companyId" value="${project.companyId}" hidden>
							<input type="hidden" class="form-control" name="companyName" value="${project.companyName}" hidden>
							<input type="hidden" class="form-control" name="employeeId" value="${project.employeeId}" hidden>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Project Name:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="projectName" value="${project.projectName}" placeholder="Project Name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Description:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="description" value="${project.description}" placeholder="Description" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Role:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="role" value="${project.role}" placeholder="Role" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Technologies:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="technologies" value="${project.technologies}" placeholder="Technologies" required>
							</div>
						</div>
						<div class='btn-toolbar'> 
							<a href="${context}/Experience/Company?id=${project.companyId}" class="btn btn-primary pull-right">Back</a> 
							<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
							<button type="submit" class="btn btn-success pull-right" form="create" value="Submit">Update</button> 
						</div>    
					</c:forEach>
				</form> 
		    </div>
	    </div>
    </div> 
</body>
</html>