<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Skill Maintenance</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body>
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
	<div class="container-fluid">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Experience Create</p></li>
				</ul>
			</div>
			<div class="panel-body"> 
				<form class="form-horizontal" action="${context}/Experience/Create" method="post" id="create">  
					<div class="form-group">
						<label class="control-label col-sm-2">Start Date:</label> 
						<div class="col-sm-10">
							<input type="date" class="form-control" name="startDate" required>
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-sm-2">End Date:</label> 
						<div class="col-sm-10">
							<input type="date" class="form-control" name="endDate" required>
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-sm-2">Company Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="companyName" placeholder="Company Name" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">Position:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="position" placeholder="Position" required>
						</div>
					</div>
					<div class='btn-toolbar'> 
						<a href="${context}/Experience" class="btn btn-primary pull-right">Back</a>
						<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button>  
						<button type="button" value="Submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#experienceAddModal">Create</button>
					</div>   
				</form>
				<!-- Experience Create Modal -->	
				<div class="modal fade" id="experienceAddModal" role="dialog">
					<div class="modal-dialog">  
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Experience Details</h4>
							</div>
							<div class="modal-body">
								<p>Confirm create experience information?</p>
							</div>
							<div class="modal-footer"> 
								<button type="submit" class="btn btn-success" form="create" value="Submit">Confirm</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div> 
					</div>
				</div>		
				<!-- Error Validation -->
				<p><strong class="text-danger">${Error_Message}</strong><strong class="text-success">${Success_Message}</strong></p> 
			</div>
		</div>
	</div>
</body>
</html>