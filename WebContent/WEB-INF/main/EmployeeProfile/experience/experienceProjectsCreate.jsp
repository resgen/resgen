<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Experience</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
	    <div class="container-fluid">
	    	<div class="panel panel-primary">
		    	<div class="panel panel-heading">
		    		<ul class="list-inline">
						<li class="glyphicon glyphicon-user c-d-s"></li>
						<li><p class="panel-title">Experience Projects Create</p></li>
					</ul>
		    	</div>
			    <div class="panel-body">
					<c:forEach items="${companyInfo}" var="company">  
						 <h2 class="text-center">${company.companyName}</h2><br> 
					</c:forEach>
					<form class="form-horizontal" action="${context}/Experience/Company/Create" method="post" id="create">  
						<c:forEach items="${companyInfo}" var="company">  
							<div class="form-group">  
								<input type="hidden" class="form-control" name="companyId" value="${company.id}" hidden>
								<input type="hidden" class="form-control" name="companyName" value="${company.companyName}" hidden>
								<input type="hidden" class="form-control" name="employeeId" value="${company.employeeId}" hidden>
							</div>
						</c:forEach>
						<div class="form-group">
							<label class="control-label col-sm-2">Project Name:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="projectName" placeholder="Project Name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Description:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="description" placeholder="Description" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Role:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="role" placeholder="Role" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Technologies:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="technologies" placeholder="Technologies" required>
							</div>
						</div>
						<div class='btn-toolbar'>
							<c:forEach items="${companyInfo}" var="company">
								<a href="${context}/Experience/Company?id=${company.id}" class="btn btn-primary pull-right">Back</a> 
								<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button>  
								<button type="button" value="Submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#educationProjectAddModal">Create</button>
							</c:forEach>
						</div>   
					</form> 
					<!-- Education Project Create Modal -->	
					<div class="modal fade" id="educationProjectAddModal" role="dialog">
						<div class="modal-dialog">  
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Project Details</h4>
								</div>
								<div class="modal-body">
									<p>Confirm create project information?</p>
								</div>
								<div class="modal-footer"> 
									<button type="submit" class="btn btn-success" form="create" value="Submit">Add</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div> 
						</div>
					</div>				 
			    </div>
		    </div>
	    </div> 
</body>
</html>