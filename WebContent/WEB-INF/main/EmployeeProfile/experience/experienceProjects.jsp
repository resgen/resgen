<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Experience</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
	    <div class="container-fluid">
	    	<div class="panel panel-primary">
		    	<div class="panel panel-heading">
		    		<ul class="list-inline">
						<li class="glyphicon glyphicon-user c-d-s"></li>
						<li><p class="panel-title">Experience Projects</p></li>
					</ul>
		    	</div>
			    <div class="panel-body">
					<c:forEach items="${companyInfo}" var="company">  
						 <h2 class="text-center">${company.companyName}</h2><br> 
						<a href="${context}/Experience" style="margin-left:5px" class="btn btn-primary pull-right">Back</a> 
						<a href="${context}/Experience/Company/Create?id=${company.id}" class="btn btn-success pull-right">Create Project</a> 
					</c:forEach>  
					<div class="form-inline">
						<div class="form-group">
							<form action="${context}/SkillMaintenance" method="get" id="search">
								<div class="input-group">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
									</div>
									<input type="text" class="form-control" name="skillSearchInput" placeholder="">
								</div>
								<button class="btn btn-primary" type="submit" form="search" value="Submit">Search</button>
							</form>
						</div>
					</div>
			    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
			    	<display:table name="projectsInfo" id="project" requestURI="${context}/Experience/Company" pagesize="5" class="table table-responsive" export="true" defaultsort="1" defaultorder="ascending"> 
					      
					    <display:column property="projectName"  sortable="true" sortName="projectName" title="Project" />
					    <display:column property="description"  sortable="true" sortName="description" title="Description" />
					    <display:column property="role"  sortable="true" sortName="role" title="Role" />
					    <display:column property="technologies"  sortable="true" sortName="technologies" title="Technologies" />  
					    <display:column title="Actions"> 
				    		<a href="${context}/Experience/Company/Project?id=${project.id}" class="btn btn-primary">Edit</a>  
					    </display:column> 
					    <display:column media="html"> 
					    	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#experienceProjectDeleteModal-${project.id}">Delete</button>
					    </display:column>
					    <display:column media="html">
						    <!-- Experience Project Delete Modal -->	
							<div class="modal fade" id="experienceProjectDeleteModal-${project.id}" role="dialog">
								<div class="modal-dialog">  
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Project Details</h4>
										</div>
										<div class="modal-body">
											<p>Confirm delete project "${project.projectName}" ?</p>
										</div>
										<div class="modal-footer"> 
											<a href="${context}/Experience/Company/Project/Delete?id=${project.id}&company=${project.companyId}" class="btn btn-success">Confirm</a>
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div> 
								</div>
							</div>
					    </display:column>
					    <display:setProperty name="export.excel.filename" value="ProjectsData.xls"/>
					    <display:setProperty name="export.pdf.filename" value="ProjectsData.pdf"/>
					    <display:setProperty name="export.rtf.filename" value="ProjectsData.rtf"/>
					    <display:setProperty name="export.csv.filename" value="ProjectsData.csv"/>   
			    	</display:table>   
			    </div>
		    </div>
	    </div> 
</body>
</html>