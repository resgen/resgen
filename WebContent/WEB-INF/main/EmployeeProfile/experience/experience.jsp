<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Experience</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
    <div class="container-fluid">
    	<div class="panel panel-primary">
	    	<div class="panel panel-heading">
	    		<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Experience</p></li>
				</ul>
	    	</div>
		    <div class="panel-body">     
		    	<a href="${context}/Experience/Create" class="btn btn-success pull-right">Create Experience</a>
		    	<div class="form-inline">
					<div class="form-group">
						<form action="${context}/Experience" method="get" id="search">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</div>
								<input type="text" class="form-control" name="experienceSearchInput" placeholder="">
							</div>
							<button class="btn btn-primary" type="submit" form="search" value="Submit">Search</button>
						</form>
					</div>
				</div>
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
		    	<display:table name="experienceInfo" id="experience" requestURI="/Experience" pagesize="5" class="table table-responsive" export="true" defaultsort="1" defaultorder="ascending"> 
				      
				    <display:column property="companyName"  sortable="true" sortName="companyName" title="Company Name" />
				    <display:column property="startDate"  sortable="true" sortName="startDate" title="Start Date" />
				    <display:column property="endDate"  sortable="true" sortName="endDate" title="End Date" />
				    <display:column property="position"  sortable="true" sortName="userType" title="Position" />  
				    <display:column title="Actions" media="html"> 
			    		<a href="${context}/Experience/Company?id=${experience.id}" class="btn btn-primary">Projects</a>  
				    </display:column> 
				    <display:column media="html"> 
				    	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#experienceDeleteModal-${experience.id}">Delete</button>
				    </display:column>
				    <display:column media="html">
					    <!-- Experience Delete Modal -->	
						<div class="modal fade" id="experienceDeleteModal-${experience.id}" role="dialog">
							<div class="modal-dialog">  
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Experience Details</h4>
									</div>
									<div class="modal-body">
										<p>Confirm delete company "${experience.companyName}" ?</p>
									</div>
									<div class="modal-footer"> 
										<a href="${context}/Experience/Delete?id=${experience.id}" class="btn btn-success">Confirm</a>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div> 
							</div>
						</div>
				    </display:column>
				    <display:setProperty name="export.excel.filename" value="ExperienceData.xls"/>
				    <display:setProperty name="export.pdf.filename" value="ExperienceData.pdf"/>
				    <display:setProperty name="export.rtf.filename" value="ExperienceData.rtf"/>
				    <display:setProperty name="export.csv.filename" value="ExperienceData.csv"/>   
		    	</display:table><br><br>   
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
		    	<a href="${context}/Personal" class="btn btn-info pull-left">Previous</a> 
		    	<a href="${context}/Skill" class="btn btn-info pull-right">Next</a>  
		    </div>
	    </div>
    </div>   
</body>
</html>