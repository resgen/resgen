<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Education</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
    <div class="container-fluid">
    	<div class="panel panel-primary">
	    	<div class="panel panel-heading">
	    		<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Education Edit</p></li>
				</ul>
	    	</div>
		    <div class="panel-body"> 
		    	<c:forEach items="${educationInfo}" var="education">  
					<form class="form-horizontal" action="${context}/Education/Edit" method="post" id="create">   
						<div class="form-group">  
							<input type="hidden" class="form-control" name="educationId" value="${education.id}" hidden>						
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">School Name:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="schoolName" value="${education.schoolName}" placeholder="School Name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Start Date:</label>
							<div class="col-sm-10">
								<input type="date" class="form-control" name="startDate" value="${education.startDate}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">End Date:</label>
							<div class="col-sm-10">
								<input type="date" class="form-control" name="endDate" value="${education.endDate}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Program:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="program" value="${education.program}" placeholder="Program" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Achievements:</label>
							<div class="col-sm-10">
								<textarea class="form-control" rows="4" maxlength="255" name="achievement"  placeholder="Achievements" required>${education.achievement}</textarea>
							</div>
						</div>
						<!-- Error Validation --> 
						<c:if test="${EducationNullError != null}">  
							<div class="col-sm-offset-2 col-sm-10 alert alert-warning fade in">
								<a href="#" class="close" data-dismiss="alert"  > &times;</a>
								<strong>Warning!</strong> ${EducationNullError}
							</div> 
						</c:if>  
						<!-- Success Validation -->
						<c:if test="${SuccessEducationUpdate != null}">  
							<div class="col-sm-offset-2 col-sm-10 alert alert-success fade in">
								<a href="#" class="close" data-dismiss="alert"  > &times;</a>
								<strong>Success!</strong> ${SuccessEducationUpdate}
							</div> 
						</c:if>
						<div class='btn-toolbar'> 
							<a href="${context}/Education" class="btn btn-primary pull-right">Back</a> 
							<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
							<button type="button" value="Submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#educationUpdateModal">Update</button>
						</div>       				
					</form> 				
					<!-- Education Create Modal -->	
					<div class="modal fade" id="educationUpdateModal" role="dialog">
						<div class="modal-dialog">  
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Education Details</h4>
								</div>
								<div class="modal-body">
									<p>Confirm update education information?</p>
								</div>
								<div class="modal-footer"> 
									<button type="submit" class="btn btn-success" form="create" value="Submit">Confirm</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div> 
						</div>
					</div>					
				</c:forEach>						
		    </div>
	    </div>
    </div> 
</body>
</html>