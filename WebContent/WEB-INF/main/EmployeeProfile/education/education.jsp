<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Education</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
    <div class="container-fluid">
    	<div class="panel panel-primary">
	    	<div class="panel panel-heading">
	    		<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Education</p></li>
				</ul>
	    	</div>
		    <div class="panel-body">     
		    	<a href="${context}/Education/Create" class="btn btn-success pull-right">Create Education</a>
		    	<div class="form-inline">
					<div class="form-group">
						<form action="${context}/Education" method="get" id="search">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</div>
								<input type="text" class="form-control" name="educationSearchInput" placeholder="">
							</div>
							<button class="btn btn-primary" type="submit" form="search" value="Submit">Search</button>
						</form>
					</div>
				</div>
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
		    	<display:table name="educationsInfo" id="education" requestURI="/Education" pagesize="5" class="table table-responsive" export="true" defaultsort="1" defaultorder="ascending"> 
				      
				    <display:column property="schoolName"  sortable="true" sortName="schoolName" title="School Name" />
				    <display:column property="startDate"  sortable="true" sortName="startDate" title="Start Date" />
				    <display:column property="endDate"  sortable="true" sortName="endDate" title="End Date" />				      
				    <display:column property="program"  sortable="true" sortName="program" title="Program" />  
				    <display:column property="achievement"  sortable="true" sortName="achievement" title="Achievements" />  
				    
				    <display:column title="Actions"> 
			    		<a href="${context}/Education/Edit?id=${education.id}" class="btn btn-primary">Edit</a>  
				    </display:column> 
				    <display:column> 
				    	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#educationDeleteModal-${education.id}">Delete</button>
				    </display:column>
				    <display:column>
					    <!-- Experience Delete Modal -->	
						<div class="modal fade" id="educationDeleteModal-${education.id}" role="dialog">
							<div class="modal-dialog">  
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Education Details</h4>
									</div>
									<div class="modal-body">
										<p>Confirm delete school "${education.schoolName}" ?</p>
									</div>
									<div class="modal-footer"> 
										<a href="${context}/Education/Delete?id=${education.id}" class="btn btn-success">Confirm</a>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div> 
							</div>
						</div>
				    </display:column>
				    <display:setProperty name="export.excel.filename" value="EducationData.xls"/>
				    <display:setProperty name="export.pdf.filename" value="EducationData.pdf"/>
				    <display:setProperty name="export.rtf.filename" value="EducationData.rtf"/>
				    <display:setProperty name="export.csv.filename" value="EducationData.csv"/>   
		    	</display:table><br><br>    
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
		    	<a href="${context}/Skill" class="btn btn-info pull-left">Previous</a> 
		    	<a href="${context}/Certification" class="btn btn-info pull-right">Next</a>  
		    </div>
	    </div>
    </div>   
</body>
</html>