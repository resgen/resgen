<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Certification</title>
  <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body>
 <jsp:directive.include file="../../constant/Layout.jsp" /> 
	<div class="container-fluid">
    	<div class="panel panel-primary">
	    	<div class="panel panel-heading">
	    		<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Certification</p></li>
				</ul>
	    	</div>
		    <div class="panel-body">     
		    	<a href="${context}/Certification/Create" class="btn btn-success pull-right">Create Certificate</a>
		    	<div class="form-inline">
					<div class="form-group">
						<form action="${context}/Certification" method="get" id="search">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</div>
								<input type="text" class="form-control" name="certificateSearchInput" placeholder="">
							</div>
							<button class="btn btn-primary" type="submit" form="search" value="Submit">Search</button>
						</form>
					</div>
				</div>
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
		    	<display:table name="certificationInfo" id="certification" requestURI="/Certification" pagesize="5" class="table table-responsive" export="true" defaultsort="1" defaultorder="ascending"> 
				    <display:column property="title"  sortable="true" sortName="middleName" title="Title" />
				    <display:column property="venue"  sortable="true" sortName="lastName" title="Venue" />
				    <display:column property="dateCert"  sortable="true" sortName="userType" title="Date" />
				    <display:column property="details"  sortable="true" sortName="userType" title="Detail" />
				    <display:column title="Actions"> 
			    		<a href="${context}/Certification/Edit?id=${certification.id}" class="btn btn-primary">Edit</a>  
				    </display:column> 
				    <display:column> 
				    	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#certificateDeleteModal-${certification.id}">Delete</button>
				    </display:column>
				     <display:column>
					    <!-- Experience Delete Modal -->	
						<div class="modal fade" id="certificateDeleteModal-${certification.id}" role="dialog">
							<div class="modal-dialog">  
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Certificate Details</h4>
									</div>
									<div class="modal-body">
										<p>Confirm delete company "${certification.title}" ?</p>
									</div>
									<div class="modal-footer"> 
										<a href="${context}/Certification/Delete?id=${certification.id}" class="btn btn-success">Confirm</a>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div> 
							</div>
						</div>
						<!-- End -->
				    </display:column>
				    <display:setProperty name="export.excel.filename" value="UserData.xls"/>
				    <display:setProperty name="export.pdf.filename" value="UserData.pdf"/>
				    <display:setProperty name="export.rtf.filename" value="UserData.rtf"/>
				    <display:setProperty name="export.csv.filename" value="UserData.csv"/>   
		    	</display:table><br><br>
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
		    	<a href="${context}/Education" class="btn btn-info pull-left">Previous</a> 
		    	<a href="" class="btn btn-info pull-right">Next</a>  
		    </div>
	    </div>
    </div>    	
</body>
</html>