<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Certification</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
    <div class="container-fluid">
    	<div class="panel panel-primary">
	    	<div class="panel panel-heading">
	    		<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Certificate Edit</p></li>
				</ul>
	    	</div>
		    <div class="panel-body">
		    	<c:forEach items="${certificationInfo}" var="certification">  
					<form class="form-horizontal" action="${context}/Certification/Edit" method="post" id="create">  
						<div class="form-group">  
								<input type="hidden" class="form-control" name="certificationId" value="${certification.id}" hidden>						
						</div> 
						<div class="form-group">
							<label class="control-label col-sm-2">Title:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="title" value="${certification.title}" placeholder="Title" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Date:</label> 
								<div class="col-sm-10">
									<input type="date"  class="form-control" value="${certification.dateCert}" name="Date_Cert"  required>
								</div>
						</div> 
						<div class="form-group">
							<label class="control-label col-sm-2">Venue:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="Venue" value="${certification.venue}" placeholder="Venue" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Details:</label>
							<div class="col-sm-10">
								<textarea class="form-control" rows = "4" style = "resize: none;" name="Details" value="${certification.details}" placeholder="Details" required>${certification.details}</textarea>
							</div>
						</div>
						<!-- Error Validation -->
						<c:if test="${CertificationNullError != null}">
							<div class="col-sm-offset-2 col-sm-10 alert alert-warning fade in">
								<a href="#" class="close" data-dismiss="alert"> &times;</a> <strong>Warning!</strong>
								${CertificationNullError}
							</div>
						</c:if>
						<!-- Success Validation -->
						<c:if test="${SuccessCertificationUpdate != null}">
							<div class="col-sm-offset-2 col-sm-10 alert alert-success fade in">
								<a href="#" class="close" data-dismiss="alert"> &times;</a> <strong>Success!</strong>
								${SuccessCertificationUpdate}
							</div>
						</c:if>
						<div class='btn-toolbar'> 
							<a href="${context}/Certification" class="btn btn-primary pull-right">Back</a> 
							<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
							<button type="button" value="Submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#certificateUpdateModal">Update</button>						    
						</div>   
					</form> 
					<!-- Certification Create Modal -->	
					<div class="modal fade" id="certificateUpdateModal" role="dialog">
						<div class="modal-dialog">  
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Certification Details</h4>
								</div>
								<div class="modal-body">
									<p>Confirm update?</p>
								</div>
								<div class="modal-footer"> 
									<button type="submit" class="btn btn-success" form="create" value="Submit">Confirm</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div> 
						</div>
					</div>
				</c:forEach>
		    </div>
	    </div>
    </div> 
</body>
</html>