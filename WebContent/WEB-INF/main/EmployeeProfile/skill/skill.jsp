<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Skill</title>
    <jsp:directive.include file="../../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../../constant/Layout.jsp" /> 
    <div class="container-fluid">
    	<div class="panel panel-primary">
	    	<div class="panel panel-heading">
	    		<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Skill</p></li>
				</ul>
	    	</div>
		    <div class="panel-body">     
		    	<a href="${context}/Skill/Create" class="btn btn-success pull-right">Create Skill</a>
		    	<div class="form-inline">
					<div class="form-group">
						<form action="${context}/Skill" method="get" id="search">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</div>
								<input type="text" class="form-control" name="skillSearchInput" placeholder="">
							</div>
							<button class="btn btn-primary" type="submit" form="search" value="Submit">Search</button>
						</form>
					</div>
				</div>
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
		    	<form action="${context}/Skill" method="post">
			    	<display:table name="activeSkills" id="skill" requestURI="/Skill" class="table table-responsive" export="true" defaultsort="1" defaultorder="ascending">
					    <display:column property="skill" sortable="true" sortName="schoolName" title="Skill Name" />   
					    <display:column title="Rating" media="html"> 
					    
					    	<%-- <c:forEach items="${personalSkill}" var="personal">
					    	
					    	</c:forEach> --%>
					    	
					    	<input type="text" name="skillId" value="${skill.id}" hidden />
					    	<input type="text" name="skillName-${skill.id}" value="${skill.skill}" hidden />
					   		<label class="radio-inline"><input type="radio" name="rating-${skill.id}" value="Others">Familiar</label>
				    		<label class="radio-inline"><input type="radio" name="rating-${skill.id}" value="1">1</label>
							<label class="radio-inline"><input type="radio" name="rating-${skill.id}" value="2">2</label>
							<label class="radio-inline"><input type="radio" name="rating-${skill.id}" value="3">3</label>
							<label class="radio-inline"><input type="radio" name="rating-${skill.id}" value="4">4</label>  
							<label class="radio-inline"><input type="radio" name="rating-${skill.id}" value="5">5</label>
							 
						</display:column>    
					    <display:setProperty name="export.excel.filename" value="SkillData.xls"/>
					    <display:setProperty name="export.pdf.filename" value="SkillData.pdf"/>
					    <display:setProperty name="export.rtf.filename" value="SkillData.rtf"/>
					    <display:setProperty name="export.csv.filename" value="SkillData.csv"/>   
			    	</display:table><br><br>
			    	<div class='btn-toolbar'> 
						<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
						<button type="submit" value="Submit" class="btn btn-success pull-right" >Update</button> 
					</div>      
		    	</form> 
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
		    	<a href="${context}/Experience" class="btn btn-info pull-left">Previous</a> 
		    	<a href="${context}/Education" class="btn btn-info pull-right">Next</a>  
		    </div>
	    </div>
    </div>   
</body>
</html>