<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Personal</title>
    <jsp:directive.include file="../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../constant/Layout.jsp" /> 
    <c:forEach items="${personalInfo}" var="personal">
	    <div class="container-fluid"> 
	    	<div class="panel panel-primary">
		    	<div class="panel panel-heading">
		    		<ul class="list-inline">
						<li class="glyphicon glyphicon-user c-d-s"></li>
						<li><p class="panel-title">Personal</p></li>
					</ul>
		    	</div>
			    <div class="panel-body">   
			   		<form class="form-horizontal" action="${context}/Personal/Profile-image" method="post" id="photo-upload" enctype="multipart/form-data">
						<div class="form-group"> 
							<label class="control-label col-sm-2">Profile Image:</label>
							<div class="col-sm-10">
								<img class="img-responsive" id="img-preview" src="${context}/assets/image/${personal.picture}"
									alt="Profile Picture" /><br> <label
									class="btn btn-success btn-file"> Browse <input
									id="file-upload-button" type="file" name="photo" size="50"
									accept="image/*" />
								</label> 
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#personalModal">Upload</button>
								</label>
							</div>
						</div>
					</form>
			    	<form class="form-horizontal" action="${context}/Personal" method="post" id="update" > 
			    			<input type="text" name="empId" value="${personal.employeeId}" hidden>			    		
							<div class="form-group">
								<label class="control-label col-sm-2">Last Name:</label> 
								<div class="col-sm-10">
									<input type="text" value="${personal.lastName}" class="form-control" name="lastName" placeholder="Last Name" required>
								</div>
							</div>  
							<div class="form-group">
								<label class="control-label col-sm-2">First Name:</label> 
								<div class="col-sm-10">
									<input type="text" value="${personal.firstName}" class="form-control" name="firstName" placeholder="First Name" required>
								</div>
							</div> 
							<div class="form-group">
								<label class="control-label col-sm-2">Middle Name:</label> 
								<div class="col-sm-10">
									<input type="text" value="${personal.middleName}" class="form-control" name="middleName" placeholder="Middle Name" required>
								</div>
							</div>  
							<div class="form-group">
								<label class="control-label col-sm-2">Home Address:</label> 
								<div class="col-sm-10">
									<input type="text" value="${personal.address}" class="form-control" name="address" placeholder="Home Address" required>
								</div>
							</div> 
							<div class="form-group">
								<label class="control-label col-sm-2">Contact Number:</label> 
								<div class="col-sm-10">
									<input type="text" value="${personal.contact}" class="form-control" name="contact" placeholder="Contact Number" required>
								</div>
							</div> 
							<div class="form-group">
								<label class="control-label col-sm-2">Email:</label> 
								<div class="col-sm-10">
									<input type="email" value="${personal.email}" class="form-control" name="email" placeholder="Email" required>
								</div>
							</div> 
							<div class="form-group">
								<label class="control-label col-sm-2">Birthday:</label> 
								<div class="col-sm-10">
									<input type="date" value="${personal.birthDate}" class="form-control" name="birthday" placeholder="Birthday" required>
								</div>
							</div> 
							<div class="form-group">
								<label class="control-label col-sm-2">Role:</label> 
								<div class="col-sm-10">
									<input type="text" value="${personal.userRole}" class="form-control" name="role" placeholder="Role" required>
								</div>
							</div> 
							<div class="form-group">
								<label class="control-label col-sm-2">Level:</label> 
								<div class="col-sm-10">
									<input type="text" value="${personal.level}" class="form-control" name="level" placeholder="Level" required>
								</div>
							</div>  
							<div class="form-group">
								<label class="control-label col-sm-2">Civil Status:</label>
								<div class="col-sm-10">
									<select name="civilStatus" class="form-control"> 
										<c:choose>
											<c:when test="${personal.civilStatus =='Single'}"> 
												<option value="Single" selected="selected">Single</option>
												<option value="Married">Married</option>
												<option value="Divorced">Divorced</option> 
												<option value="Widowed">Widowed</option>  
											</c:when>
											<c:when test="${personal.civilStatus== 'Married'}">
												<option value="Single">Single</option>
												<option value="Married" selected="selected">Married</option>
												<option value="Divorced">Divorced</option> 
												<option value="Widowed">Widowed</option>  
											</c:when>
											<c:when test="${personal.civilStatus== 'Divorced'}">
												<option value="Single">Single</option>
												<option value="Married">Married</option>
												<option value="Divorced" selected="selected">Divorced</option> 
												<option value="Widowed">Widowed</option>  
											</c:when>
											<c:otherwise>
												<option value="Single">Single</option>
												<option value="Married">Married</option>
												<option value="Divorced">Divorced</option> 
												<option value="Widowed" selected="selected">Widowed</option>  
											</c:otherwise>
										</c:choose>
									</select>
								</div>
							</div>   
							<div class="form-group">
								<label class="control-label col-sm-2">Overview:</label> 
								<div class="col-sm-10">
									<textarea rows="4" name="overview" maxlength="255" class="form-control" placeholder="Overview" required>${personal.overView}</textarea> 
								</div>
							</div>  
							<div class='btn-toolbar'> 
								<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
								<button type="button" value="Submit" class="btn btn-primary pull-right" data-toggle="modal" data-target="#personalDetailsModal">Update</button> 
							</div>  
					</form>  
			    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
			    	<a href="${context}/Experience" class="btn btn-info pull-right">Next</a>  
			    </div>
		    </div>
	    </div>
	    
	    <!-- Photo Upload Modal -->	
		<div class="modal fade" id="personalModal" role="dialog">
			<div class="modal-dialog">  
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Profile Picture</h4>
					</div>
					<div class="modal-body">
						<p>Upload photo?</p>
					</div>
					<div class="modal-footer"> 
						<label>
							<label class="btn btn-primary btn-file"> Confirm <input id="file-save-button" type="submit" form="photo-upload" value="Change photo">
						</label> 
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div> 
			</div>
		</div>
		
		<!-- Personal Update Modal -->	
		<div class="modal fade" id="personalDetailsModal" role="dialog">
			<div class="modal-dialog">  
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Profile Details</h4>
					</div>
					<div class="modal-body">
						<p>Update personal details?</p>
					</div>
					<div class="modal-footer"> 
						<button type="submit" class="btn btn-success " form="update" value="Submit">Confirm</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div> 
			</div>
		</div>
		
    </c:forEach>
</body>
</html>