<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Home</title>
    <jsp:directive.include file="constant/Head.jsp" /> 
</head>
<body>
    <jsp:directive.include file="constant/Layout.jsp" />
    <h1 class="text-center">Main</h1>
</body>
</html>