<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Skill Maintenance</title>
    <jsp:directive.include file="../constant/Head.jsp" /> 
</head>
<body>
    <jsp:directive.include file="../constant/Layout.jsp" /> 
	<div class="container-fluid">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Skill Maintenance Create</p></li>
				</ul>
			</div>
			<div class="panel-body"> 
				<form class="form-horizontal" action="${context}/SkillMaintenance/Create" method="post" id="create"> 
					<div class="form-group">
						<label class="control-label col-sm-2">Skill:</label> 
						<div class="col-sm-10">
							<input type="text" class="form-control" name="skillName" placeholder="Skill Name" required>
						</div>
					</div>   
					<div class='btn-toolbar'>
						<a href="${context}/SkillMaintenance" class="btn btn-primary pull-right">Back</a>
						<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
						<button type="submit" class="btn btn-success pull-right" form="create" value="Submit">Create</button>
					</div>
				</form>  
				<!-- Error Validation -->
				<p><strong class="text-danger">${Error_Message}</strong><strong class="text-success">${Success_Message}</strong></p> 
			</div>
		</div>
	</div>
</body>
</html>