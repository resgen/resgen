<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>User Maintenance</title>
    <jsp:directive.include file="../constant/Head.jsp" /> 
</head>
<body>
    <jsp:directive.include file="../constant/Layout.jsp" /> 
	<div class="container-fluid">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">User Maintenance Edit</p></li>
				</ul>
			</div>
			<div class="panel-body"> 
				<form class="form-horizontal" action="${context}/UserMaintenance/User" method="post" id="edit"> 
					<c:forEach items="${userInfo}" var="user">
						<input type="hidden" name="userId" value="${user.ID}">
						<div class="form-group">
							<label class="control-label col-sm-2">Employee ID:</label> 
							<div class="col-sm-10">
								<input type="text" class="form-control" name="employeeId" placeholder="Employee ID" value="${user.employeeId}" required>
							</div>
						</div> 
						<div class="form-group">
							<label class="control-label col-sm-2">First Name:</label>
							<div class="col-sm-10"> 
								<input type="text" class="form-control" name="firstName" placeholder="First Name" value="${user.firstName}" required> 
							</div>
						</div> 
						<div class="form-group">
							<label class="control-label col-sm-2">Middle Name:</label>
							<div class="col-sm-10"> 
								<input type="text" class="form-control" name="middleName" placeholder="Middle Name" value="${user.middleName}" required> 
							</div>
						</div> 
						<div class="form-group">
							<label class="control-label col-sm-2">Last Name:</label>
							<div class="col-sm-10"> 
								<input type="text" class="form-control" name="lastName" placeholder="Last Name" value="${user.lastName}" required> 
							</div>
						</div> 
						<div class="form-group">
							<label class="control-label col-sm-2">Email:</label>
							<div class="col-sm-10"> 
								<input type="email" class="form-control" name="email" placeholder="Email" value="${user.email}" required>  
							</div>
						</div> 
						<div class="form-group">
							<label class="control-label col-sm-2">Status:</label>
							<div class="col-sm-10">
								<input type="checkbox" name="userStatus" checked data-toggle="toggle" data-on="Activate" data-off="Deactivate">
							</div>
						</div>  
						<div class="form-group">
							<label class="control-label col-sm-2">User Type:</label>
							<div class="col-sm-10">
								<select name="userType" class="form-control"> 
									<c:choose>
									    <c:when test="${user.userType == 'Admin'}">
									        <option selected="selected" value="Admin">Admin</option>
											<option value="Employee">Employee</option>
											<option value="Guest">Guest</option> 
									    </c:when>
									    <c:when test="${user.userType == 'Employee'}">
									        <option value="Admin">Admin</option>
											<option selected="selected" value="Employee">Employee</option>
											<option value="Guest">Guest</option> 
									    </c:when>
									    <c:otherwise>
									        <option value="Admin">Admin</option>
											<option value="Employee">Employee</option>
											<option selected="selected" value="Guest">Guest</option> 
									    </c:otherwise>
									</c:choose>
								</select>
							</div>
						</div> 
						<div class='btn-toolbar'>
							<a href="${context}/UserMaintenance" class="btn btn-primary pull-right">Back</a>
							<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
							<button type="submit" class="btn btn-success pull-right" form="edit" value="Submit">Update</button>
						</div>
					</c:forEach>
				</form>  
				<!-- Error Validation -->
				<p><strong class="text-danger">${Error_Message}</strong><strong class="text-success">${Success_Message}</strong></p> 
			</div>
		</div>
	</div>
</body>
</html>