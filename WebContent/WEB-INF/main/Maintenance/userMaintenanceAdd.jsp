<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>User Maintenance</title>
    <jsp:directive.include file="../constant/Head.jsp" /> 
</head>
<body>
    <jsp:directive.include file="../constant/Layout.jsp" /> 
	<div class="container-fluid">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">User Maintenance Create</p></li>
				</ul>
			</div>
			<div class="panel-body"> 
				<form class="form-horizontal" action="${context}/UserMaintenance/Create" method="post" id="create"> 
					<div class="form-group">
						<label class="control-label col-sm-2">Employee ID:</label> 
						<div class="col-sm-10">
							<input type="text" class="form-control" name="employeeId" placeholder="Employee ID" required>
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-sm-2">First Name:</label>
						<div class="col-sm-10"> 
							<input type="text" class="form-control" name="firstName" placeholder="First Name" required> 
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-sm-2">Middle Name:</label>
						<div class="col-sm-10"> 
							<input type="text" class="form-control" name="middleName" placeholder="Middle Name" required> 
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-sm-2">Last Name:</label>
						<div class="col-sm-10"> 
							<input type="text" class="form-control" name="lastName" placeholder="Last Name" required> 
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-sm-2">Email:</label>
						<div class="col-sm-10"> 
							<input type="email" class="form-control" name="email" placeholder="Email" required>  
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-sm-2">User Type:</label>
						<div class="col-sm-10">
							<select name="userType" class="form-control">
								<option value="Admin">Admin</option>
								<option value="Employee">Employee</option>
								<option value="Guest">Guest</option> 
							</select>
						</div>
					</div> 
					<br> 
					<div class='btn-toolbar'>
						<a href="${context}/UserMaintenance" class="btn btn-primary pull-right">Back</a>
						<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
						<button type="submit" class="btn btn-success pull-right" form="create" value="Submit">Create</button>
					</div>
				</form>  
				<!-- Error Validation -->
				<p><strong class="text-danger">${Error_Message}</strong><strong class="text-success">${Success_Message}</strong></p> 
			</div>
		</div>
	</div>
</body>
</html>