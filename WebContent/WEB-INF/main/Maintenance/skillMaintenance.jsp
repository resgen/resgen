<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Skill Maintenance</title>
    <jsp:directive.include file="../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../constant/Layout.jsp" /> 
    <div class="container-fluid">
    	<div class="panel panel-primary">
			<div class="panel panel-heading">
				<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Skill Maintenance</p></li>
				</ul>
			</div>
			<div class="panel-body">
				<a href="${context}/SkillMaintenance/Create" class="btn btn-success pull-right">Create Skill</a>
				<div class="form-inline">
					<div class="form-group">
						<form action="${context}/SkillMaintenance" method="get" id="search">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</div>
								<input type="text" class="form-control" name="skillSearchInput" placeholder="">
							</div>
							<button class="btn btn-primary" type="submit" form="search" value="Submit">Search</button>
						</form>
					</div>
				</div>
				<hr style="height: 2px; border: none; color: #333; background-color: #333;" />
				<display:table name="skills" id="skill" requestURI="/SkillMaintenance" pagesize="10" class="table table-responsive" export="true" defaultsort="1" defaultorder="ascending"> 
					<display:column property="skill" sortable="true" sortName="skill" title="Skill" /> 
					<display:column property="active" sortable="true" sortName="active" title="Status" />
					<display:column title="Actions" media="html">
						<c:if test="${skill.active=='Active'}">
							<a href="SkillMaintenance/Skill?id=${skill.id}"
								class="btn btn-primary">Edit</a>
						</c:if>
					</display:column>
					<display:column media="html">
				    	<c:choose>
					        <c:when test="${skill.active=='Inactive'}">
					        	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#statusModal-${skill.id}">Activate</button>
					        </c:when>
					        <c:otherwise>
					        	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#statusModal-${skill.id}">Deactivate</button>
					        </c:otherwise>
				        </c:choose>  
				        
						<!-- Modal -->	
						<div class="modal fade" id="statusModal-${skill.id}" role="dialog">
							<div class="modal-dialog"> 
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Status</h4>
									</div>
									<div class="modal-body">
										<p>Change skill status?</p>
									</div>
									<div class="modal-footer">
										<c:choose>
											<c:when test="${skill.active=='Inactive'}">
												<button id="Activate" type="button" skill-id="${skill.id}" class="btn btn-success" data-dismiss="modal">Confirm</button>
											</c:when>
											<c:otherwise> 
												<button id="Deactivate" type="button" skill-id="${skill.id}" class="btn btn-success" data-dismiss="modal">Confirm</button>
											</c:otherwise>
										</c:choose> 
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div> 
							</div>
						</div>
					</display:column>
					<display:setProperty name="export.excel.filename" value="SkillMaintenance.xls" />
					<display:setProperty name="export.pdf.filename" value="SkillMaintenance.pdf" />
					<display:setProperty name="export.rtf.filename" value="SkillMaintenance.rtf" />
					<display:setProperty name="export.csv.filename" value="SkillMaintenance.csv" />
				</display:table>
			</div>
		</div>
    </div>
</body>
</html>