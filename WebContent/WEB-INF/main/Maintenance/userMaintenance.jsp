<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>User Maintenance</title>
    <jsp:directive.include file="../constant/Head.jsp" /> 
</head>
<body> 
    <jsp:directive.include file="../constant/Layout.jsp" /> 
    <div class="container-fluid">
    	<div class="panel panel-primary">
	    	<div class="panel panel-heading">
	    		<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">User Maintenance</p></li>
				</ul>
	    	</div>
		    <div class="panel-body"> 
		    	<a href="${context}/UserMaintenance/Create" class="btn btn-success pull-right">Create User</a> 
		    	<div class="form-inline"> 
		    		<div class="form-group">
		    			<form action="${context}/UserMaintenance" method="get" id="search">
							<div class="input-group">
								<div class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
								<input type="text" class="form-control" name="userSearchInput" placeholder=""> 
							</div>
							<button class="btn btn-primary" type="submit" form="search" value="Submit">Search</button> 
						</form>
					</div> 
		    	</div>   
		    	<hr style="height:2px;border:none;color:#333;background-color:#333;" />
			    <display:table name="users" id="user" requestURI="/UserMaintenance" pagesize="10" class="table table-responsive" export="true" defaultsort="1" defaultorder="ascending"> 
				     
				    <display:column property="employeeId" sortable="true" sortName="employeeId" title="Employee ID" /> 
				    <display:column property="firstName"  sortable="true" sortName="firstName" title="First Name" />
				    <display:column property="middleName"  sortable="true" sortName="middleName" title="Middle Name" />
				    <display:column property="lastName"  sortable="true" sortName="lastName" title="Last Name" />
				    <display:column property="userType"  sortable="true" sortName="userType" title="User Type" />
				    <display:column property="email"  sortable="true" sortName="email" title="Email" />
				    <display:column property="active" sortable="true" sortName="active" title="Status" /> 
				    <display:column title="Actions" media="html">
				    	<c:if test="${user.active=='Active'}">
				    		<a href="UserMaintenance/User?id=${user.ID}" class="btn btn-primary">Edit</a>
				    	</c:if> 
				    </display:column> 
				    <display:column media="html">
				    	<c:choose>
					        <c:when test="${user.active=='Inactive'}">
					        	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#statusModal-${user.employeeId}">Activate</button>
					        </c:when>
					        <c:otherwise>
					        	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#statusModal-${user.employeeId}">Deactivate</button>
					        </c:otherwise>
				        </c:choose>  
				        
						<!-- Modal -->
						<div class="modal fade" id="statusModal-${user.employeeId}" role="dialog">
							<div class="modal-dialog"> 
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Status</h4>
									</div>
									<div class="modal-body">
										<p>Change user status?</p>
									</div>
									<div class="modal-footer">
										<c:choose>
											<c:when test="${user.active=='Inactive'}">
												<button id="Activate" type="button" user-id="${user.employeeId }" class="btn btn-success" data-dismiss="modal">Confirm</button>
											</c:when>
											<c:otherwise> 
												<button id="Deactivate" type="button" user-id="${user.employeeId }" class="btn btn-success" data-dismiss="modal">Confirm</button>
											</c:otherwise>
										</c:choose> 
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Close</button>
									</div>
								</div> 
							</div>
						</div>
					</display:column>
				    <display:setProperty name="export.excel.filename" value="UserMaintenance.xls"/>
				    <display:setProperty name="export.pdf.filename" value="UserMaintenance.pdf"/>
				    <display:setProperty name="export.rtf.filename" value="UserMaintenance.rtf"/>
				    <display:setProperty name="export.csv.filename" value="UserMaintenance.csv"/>   
			    </display:table> 
				
			</div>
	    </div>
    </div>
</body>
</html>