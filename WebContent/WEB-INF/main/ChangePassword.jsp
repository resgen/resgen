<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head> 
	<title>Skill Maintenance</title>
    <jsp:directive.include file="constant/Head.jsp" /> 
</head>
<body>
    <jsp:directive.include file="constant/Layout.jsp" /> 
	<div class="container-fluid">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				<ul class="list-inline">
					<li class="glyphicon glyphicon-user c-d-s"></li>
					<li><p class="panel-title">Change Password</p></li>
				</ul>
			</div>
			<div class="panel-body"> 
				<form class="form-horizontal" action="${context}/Change-password" method="post" id="change"> 
					<div class="form-group">
						<label class="control-label col-sm-2">Current Password:</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" name="currentPassword" placeholder="Current Password" required>
						</div> 
					</div> 
					<!-- Error Validation -->
					<c:if test="${CurrentPasswordError != null}">
						<div class="form-group"> 
							<div class="alert alert-warning fade in col-md-offset-2 col-sm-3">
								<a href="#" class="close" data-dismiss="alert"> &times;</a> <strong>Warning!</strong>
								${CurrentPasswordError}
							</div>
						</div>  
					</c:if>
					<div class="form-group">
						<label class="control-label col-sm-2">New Password:</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" name="newPassword" placeholder="New Password" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">Confirm Password:</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password" required>
						</div>
					</div> 
					<!-- Error Validation -->
					<c:if test="${ConfirmPasswordError != null}">
						<div class="form-group"> 
							<div class="alert alert-warning fade in col-md-offset-2 col-sm-3">
								<a href="#" class="close" data-dismiss="alert"> &times;</a> <strong>Warning!</strong>
								${ConfirmPasswordError}
							</div>
						</div>  
					</c:if>
					<!-- Error Validation -->
					<c:if test="${PasswordNullError != null}">
						<div class="form-group">
							<div class="alert alert-warning fade in col-md-offset-2 col-sm-3">
								<a href="#" class="close" data-dismiss="alert"> &times;</a> <strong>Warning!</strong>
								${PasswordNullError}
							</div>
						</div>
					</c:if>
					<!-- Success Validation -->
					<c:if test="${PasswordChange != null}">
						<div class="form-group">
							<div class="alert alert-success fade in col-md-offset-2 col-sm-3">
								<a href="#" class="close" data-dismiss="alert"> &times;</a> <strong>Success!</strong>
								${PasswordChange}
							</div>
						</div>
					</c:if>
					<div class='btn-toolbar'>  
						<button type="reset" class="btn btn-warning pull-right" value="Reset">Cancel</button> 
						<button type="button" value="Submit" class="btn btn-primary pull-right" data-toggle="modal" data-target="#confirmPassword">Change</button> 
					</div>   
				</form> 
				<!-- Modal -->
				<div class="modal fade" id="confirmPassword" role="dialog">
					<div class="modal-dialog"> 
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Status</h4>
							</div>
							<div class="modal-body">
								<p>Change password?</p>
							</div>
							<div class="modal-footer"> 
								<button type="submit" class="btn btn-success" form="change" value="Submit">Confirm</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>