<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="${context}/Home">Employee Profile</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="${context}/Home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
			<c:if test="${sessionScope.SessionUserType == 'Admin'}">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-wrench"></span> Maintenance <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="${context}/UserMaintenance">User Maintenance</a></li>
						<li><a href="${context}/SkillMaintenance">Skill Maintenance</a></li>
					</ul>
				</li>
			</c:if>
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list-alt"></span> Profile Resume <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="${context}/Personal">Personal</a></li>
					<li><a href="${context}/Experience">Experience</a></li>
					<li><a href="${context}/Skill">Skill</a></li>
					<li><a href="${context}/Education">Education</a></li>
					<li><a href="${context}/Certification">Certification</a></li>
					<li><a href="${context}/Experience">Seminar</a></li>
				</ul>
			</li> 
		</ul>
		<ul class="nav navbar-nav navbar-right"> 
			<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-cog"></span> Settings <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="${context}/Change-password">Change Password</a></li> 
				</ul>
			</li> 
			<li><a href="${context}/Logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
		</ul>
	</div>
</nav>