<script src="${context}/assets/jquery/jquery-2.2.4.min.js"></script>
<script src="${context}/assets/bootstrap336/js/bootstrap.js"></script>
<script src="${context}/assets/bootstrap336/js/bootstrap-toggle.min.js"></script>
<script src="${context}/assets/js/profile-image-preview.js"></script>
<script src="${context}/assets/js/ajaxUserStatus.js"></script>
<script src="${context}/assets/js/ajaxSkillStatus.js"></script>  
<script src="${context}/assets/js/tool-tip.js"></script>
<script src="${context}/assets/js/alerts/successRegister.js"></script>    
<link rel="stylesheet" type="text/css" href="${context}/assets/bootstrap336/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${context}/assets/bootstrap336/css/bootstrap-toggle.min.css">
<link rel="stylesheet" type="text/css" href="${context}/assets/css/app.css"> 
<link rel="stylesheet" type="text/css" href="${context}/assets/css/util/tool-tip.css"> 