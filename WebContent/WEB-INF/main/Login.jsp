<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head>
    <title>Login</title>
    <jsp:directive.include file="constant/Head.jsp" /> 
</head>
<body>
    <div class="form-page container-fluid">
        <div class="form">
            <h1>Employee Profile</h1>
            <form action="Login-process" method="post" id="Login">
                <input type="text" name="employeeId" placeholder="Employee ID" required> 
                <input type="password" name="passWord" placeholder="Password" required>
                <button type="submit" form="Login" value="Submit">Login</button>
            </form>
            <a href="Register">New user? Create an account</a>
            <!-- Error Validation --> 
            <c:if test="${LoginError != null}">  
				<div class="alert alert-warning fade in">
					<a href="#" class="close" data-dismiss="alert"  > &times;</a>
					<strong>Warning!</strong> ${LoginError}
				</div> 
			</c:if>
        </div>
    </div>
</body>
</html>