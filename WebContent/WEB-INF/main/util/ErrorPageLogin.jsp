<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>404 Not Found</title>
<jsp:directive.include file="../constant/Head.jsp" /> 
</head>
<body>
<jsp:directive.include file="../constant/Layout.jsp" /> 
<h1 class="text-center"> ERROR: 404 NOT FOUND</h1>
</body>
</html>