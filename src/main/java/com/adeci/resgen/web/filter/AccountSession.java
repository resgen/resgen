package com.adeci.resgen.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.adeci.resgen.controller.util.VariableConstants.*;

/**
 * Servlet Filter implementation class AccountSession
 */
@WebFilter(filterName="AccountSessionFilter", urlPatterns="/*")
public class AccountSession implements Filter { 
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	public void destroy() { 
	} 
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession(); 
		String URL = request.getServletPath(); 
		CONTEXT = request.getContextPath();
		boolean isLoggedIn = session != null && session.getAttribute("SessionEmployeeId") != null; 
		boolean isAdmin = session != null && session.getAttribute("SessionUserType") != null && session.getAttribute("SessionUserType").equals("Admin"); 
	 
		//URL assigned here wont be redirected to login if user is not Logged in , <!> ONLY USE THIS FOR FILES USE GLOBALLY <!>
		if(URL.startsWith(ASSETS)  ){  
			chain.doFilter(request, response);
			
		//URL assigned here doesn't need the user to be logged in but if the user is logged in then invalidate the session <!> ONLY USE THIS FOR FILES/SERVLETS THAT DOESN'T NEED A SESSION <!>
		} else if(URL.equals(LOGIN_SERVLET) || URL.equals(CONTEXT) || URL.equals(LOGOUT_SERVLET) ||  URL.equals(REGISTER_SERVLET) || URL.equals(LOGIN_PROCESS_SERVLET)){
			if (isLoggedIn) {
				session.invalidate();  
			}
			chain.doFilter(request, response);
			
		//Check if user is Logged in and if not then redirect to Login page
		} else if (isLoggedIn) { 
			//Check if user is Admin to access this servlets
			if((URL.startsWith(USER_MAINTENANCE_SERVLET) || URL.startsWith(SKILL_MAINTENANCE_SERVLET))  && !isAdmin ){ 
				response.sendRedirect(CONTEXT+ERROR_404_SERVLET);
			} else {
				chain.doFilter(request, response);
			}  
		} else {
			response.sendRedirect(CONTEXT+LOGIN_SERVLET);
		} 
	} 
} 