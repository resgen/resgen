package com.adeci.resgen.web.filter;

import java.io.IOException;

/**
 * Created by Siegfrid on 6/27/2016.
 */
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName="NoCacheFilter", urlPatterns="/*")
public class NoCache implements Filter {
	public void init(FilterConfig config) throws ServletException {

	}
	public void destroy() {

	}

	public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain) {
		try {
			if (response instanceof HttpServletResponse) {
				HttpServletResponse httpresponse = (HttpServletResponse)response ;
				// Set the Cache-Control and Expires header
				httpresponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
				httpresponse.setHeader("Pragma", "no-cache"); 
				httpresponse.setDateHeader("Expires", 0); 
				// Print out the URL filtering
				/*String name = ((HttpServletRequest)request).getRequestURI();
				System.out.println("No Cache Filtering: " + name) ;*/
			}
			chain.doFilter (request, response);
		} catch (IOException e) {
			System.out.println ("IOException in NoCacheFilter");
			e.printStackTrace() ;
		} catch (ServletException e) {
			System.out.println ("ServletException in NoCacheFilter");
			e.printStackTrace() ;
		}
	}
}