package com.adeci.resgen.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.adeci.resgen.config.DatabaseConfiguration;
import com.adeci.resgen.domain.PersonalSkill;
import com.adeci.resgen.domain.Skill;  


public class SkillRepository {

    private Connection connection;

    public SkillRepository() {
        connection = DatabaseConfiguration.getConnection();
    }

    public void addSkill(Skill skill) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO skillmaintenance" +

                            "(Skill,Active)" +

                            " VALUES (?, ?, ?)");
            
            preparedStatement.setString(1, skill.getSkill()); 
            preparedStatement.setString(2, skill.getActive());
            preparedStatement.executeUpdate();
          
        } catch (SQLException e) {
            e.printStackTrace();
        }
    } 
    
    public void updateSkill(Skill skill) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE skillmaintenance " 
                    					+ "	 SET Skill = ?,"
                    					+ "		 Active = ? " 
                    					+ "WHERE ID = ? ");

            preparedStatement.setString(1, skill.getSkill());
            preparedStatement.setString(2, skill.getActive()); 
            preparedStatement.setInt(3, skill.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }  
    
    public void updatePersonalSkill(PersonalSkill personalSkill) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO skill (skillId,employee_id,skill,active,rate) " 
                    					+ "		VALUES ( ?, ?, ?, ?, ?)" 
                    					+ "		    ON DUPLICATE KEY "
                    					+ "		UPDATE employee_id=?, skillId=?, skill=?, active=?, rate=?");
            
            //Insert
            preparedStatement.setInt(1, personalSkill.getSkillId());
            preparedStatement.setString(2, personalSkill.getEmployeeId());
            preparedStatement.setString(3, personalSkill.getSkill());
            preparedStatement.setString(4, personalSkill.getActive());
            preparedStatement.setString(5, personalSkill.getRating());
            //Update
            preparedStatement.setString(6, personalSkill.getEmployeeId());
            preparedStatement.setInt(7, personalSkill.getSkillId());
            preparedStatement.setString(8, personalSkill.getSkill());
            preparedStatement.setString(9, personalSkill.getActive());
            preparedStatement.setString(10, personalSkill.getRating());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updateSkillMaintenanceStatus(int id,String status) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE skillmaintenance " 
                    					+ "	 SET Active = ? " 
                    					+ "WHERE ID = ? ");

            preparedStatement.setString(1, status);
            preparedStatement.setInt(2, id); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List<Skill> getAllSkills() {
        List<Skill> skills = new ArrayList<Skill>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM skillmaintenance");
            while (rs.next()) {
            	Skill skill = new Skill();
            	skill.setId(rs.getInt("ID")); 
            	skill.setSkill(rs.getString("Skill")); 
            	skill.setActive(rs.getString("Active"));
            	skills.add(skill);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return skills;
    }
    
    public List<Skill> getAllActiveSkills() {
        List<Skill> skills = new ArrayList<Skill>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM skillmaintenance"
            										+ "	   WHERE Active = 'Active' ");
            while (rs.next()) {
            	Skill skill = new Skill();
            	skill.setId(rs.getInt("ID")); 
            	skill.setSkill(rs.getString("Skill")); 
            	skill.setActive(rs.getString("Active"));
            	skills.add(skill);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return skills;
    }
    
    public List<PersonalSkill> getUserSkillByEmployeeId(String empId) {
        List<PersonalSkill> personalSkills = new ArrayList<PersonalSkill>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM skill " 
										+ "  	 WHERE employee_id = ?"
										+ "		   AND active = 'Active' ");

            preparedStatement.setString(1, empId); 
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {  
            	PersonalSkill personalSkill = new PersonalSkill(); 
            	personalSkill.setSkillId(rs.getInt("skillId"));
            	personalSkill.setEmployeeId(rs.getString("employee_id"));
            	personalSkill.setSkill(rs.getString("skill"));
            	personalSkill.setActive(rs.getString("active"));
            	personalSkill.setRating(rs.getString("rate"));
            	personalSkills.add(personalSkill);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personalSkills;
    }
    
    public List<Skill> getSkillMaintenanceById(int id) {
    	Skill skill = new Skill();
        List<Skill> skills = new ArrayList<Skill>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM skillmaintenance " 
										+ "  	 WHERE ID = ? "
										+ "		   AND Active = 'Active' ");

            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {   
            	skill.setId(rs.getInt("ID"));
            	skill.setSkill(rs.getString("Skill")); 
            	skill.setActive(rs.getString("Active"));
            	skills.add(skill);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return skills;
    }
    
    public List<Skill> getSkillMaintenanceBySkill(String id) {
        List<Skill> skills = new ArrayList<Skill>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM skillmaintenance " 
										+ "  	 WHERE Skill like ? "
										+ "			OR Active like ? "
										+ "	  GROUP BY Skill ");

            preparedStatement.setString(1, "%" + id + "%");
            preparedStatement.setString(2, "%" + id + "%"); 
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {  
            	Skill skill = new Skill();
            	skill.setId(rs.getInt("ID")); 
            	skill.setSkill(rs.getString("Skill"));
            	skill.setActive(rs.getString("Active")); 
                skills.add(skill);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return skills;
    }
    
    public boolean isSkillMaintenanceIdActive(int id) { 
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * " 
                    					+ "		FROM skillmaintenance WHERE ID = ?"
                    					+ "	 	 AND Active = 'Active' ");
 
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) { 
                return true; 
            }  
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
}