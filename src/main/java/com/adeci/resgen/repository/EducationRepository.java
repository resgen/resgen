package com.adeci.resgen.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.adeci.resgen.config.DatabaseConfiguration;
import com.adeci.resgen.domain.Education;
import com.adeci.resgen.domain.UserProject;  


public class EducationRepository {

    private Connection connection;

    public EducationRepository() {
        connection = DatabaseConfiguration.getConnection();
    }

    public void addEducation(Education education) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO education" +

                            "(employee_id,start_date,end_date,school_name,program,achievement)" +

                            " VALUES (?, ?, ?, ?, ?, ?)");
            
            preparedStatement.setString(1, education.getEmployeeId());
            preparedStatement.setString(2, education.getStartDate());
            preparedStatement.setString(3, education.getEndDate());
            preparedStatement.setString(4, education.getSchoolName());
            preparedStatement.setString(5, education.getProgram());
            preparedStatement.setString(6, education.getAchievement());
            preparedStatement.executeUpdate();
          
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updateEducation(Education education) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE education " 
                    					+ "	 	  SET school_name = ?,"
                    					+ "	     	  start_date = ?,"
                    					+ "		 	  end_date = ?, "
                    					+ "		 	  program = ?, "
                    					+ "			  achievement = ? "
                    					+ "		WHERE employee_id = ? "
                    					+ "		  AND id = ?");
           
            preparedStatement.setString(1, education.getSchoolName());
            preparedStatement.setString(2, education.getStartDate());
            preparedStatement.setString(3, education.getEndDate());
            preparedStatement.setString(4, education.getProgram());
            preparedStatement.setString(5, education.getAchievement());
            preparedStatement.setString(6, education.getEmployeeId());
            preparedStatement.setInt(7, education.getId());  
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteEducation(int id) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM education "  
                    					+ "WHERE id = ?");
 
            preparedStatement.setInt(1, id); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List<Education> getAllEducations() {
        List<Education> educations = new ArrayList<Education>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM education");
            while (rs.next()) {
            	Education education = new Education();
            	education.setId(rs.getInt("id")); 
            	education.setEmployeeId(rs.getString("employee_id")); 
            	education.setStartDate(rs.getString("start_date"));
            	education.setEndDate(rs.getString("end_date"));
            	education.setSchoolName(rs.getString("school_name"));
            	education.setProgram(rs.getString("program"));
            	education.setAchievement(rs.getString("achievement"));
            	educations.add(education);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return educations;
    }
    
    public List<Education> getEducationById(int id, String employeeId) {
        List<Education> educations = new ArrayList<Education>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM education " 
										+ "		 WHERE id = ? "
										+ "		   AND employee_id = ?");

            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, employeeId); 
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {     
            	Education education = new Education();
            	education.setId(rs.getInt("id"));
            	education.setEmployeeId(rs.getString("employee_id"));
            	education.setStartDate(rs.getString("start_date"));
            	education.setEndDate(rs.getString("end_date"));
            	education.setSchoolName(rs.getString("school_name"));
            	education.setProgram(rs.getString("program"));
            	education.setAchievement(rs.getString("achievement"));
            	educations.add(education);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return educations;
    }
    
    public List<Education> getEducationByEmployeeId(String employeeId) {
        List<Education> educations = new ArrayList<Education>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM education " 
										+ "		 WHERE employee_id = ?");										

            preparedStatement.setString(1, employeeId);            
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {     
            	Education education = new Education();
            	education.setId(rs.getInt("id"));
            	education.setEmployeeId(rs.getString("employee_id"));
            	education.setStartDate(rs.getString("start_date"));
            	education.setEndDate(rs.getString("end_date"));
            	education.setSchoolName(rs.getString("school_name"));
            	education.setProgram(rs.getString("program"));
            	education.setAchievement(rs.getString("achievement"));
            	educations.add(education);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return educations;
    }
    
    public List<Education> getEducationSearchByEmployeeId(String search , String employeeId) {
        List<Education> educations = new ArrayList<Education>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM education " 
										+ "		 WHERE (start_date like ? "
										+ "         OR end_date like ? "
										+ "         OR school_name like ? "
										+ "         OR program like ? "
										+ "         OR achievement like ?) "
										+ "		   AND employee_id = ? ");										

            preparedStatement.setString(1, "%" + search + "%");
            preparedStatement.setString(2, "%" + search + "%");
            preparedStatement.setString(3, "%" + search + "%");
            preparedStatement.setString(4, "%" + search + "%");
            preparedStatement.setString(5, "%" + search + "%");
            preparedStatement.setString(6, employeeId);            
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {     
            	Education education = new Education();
            	education.setId(rs.getInt("id"));
            	education.setEmployeeId(rs.getString("employee_id"));
            	education.setStartDate(rs.getString("start_date"));
            	education.setEndDate(rs.getString("end_date"));
            	education.setSchoolName(rs.getString("school_name"));
            	education.setProgram(rs.getString("program"));
            	education.setAchievement(rs.getString("achievement"));
            	educations.add(education);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return educations;
    }
    
    public boolean isEducationFromEmployee(String employeeId, int id) { 
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * " 
                            			+ "		FROM education WHERE employee_id=?"
                            			+ "		 AND id = ?");

            preparedStatement.setString(1, employeeId);
            preparedStatement.setInt(2, id);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) { 
                return true; 
            }  
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }    
    
}