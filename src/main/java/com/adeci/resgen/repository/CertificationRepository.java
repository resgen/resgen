package com.adeci.resgen.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.adeci.resgen.config.DatabaseConfiguration;
import com.adeci.resgen.domain.Certificate;
import com.adeci.resgen.domain.Education;
import com.adeci.resgen.domain.Experience;
import com.adeci.resgen.domain.PersonalAndUser;

public class CertificationRepository {
	private Connection connection;

    public CertificationRepository() {
        connection = DatabaseConfiguration.getConnection();
    }
    
    public void updateCertification(Certificate certificate) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE certification " 
                    					+ "	 	  SET title = ?,"
                    					+ "	     	  date_cert = ?,"
                    					+ "		 	  venue = ?, "
                    					+ "		 	  details = ? "
                    					+ "		WHERE employee_id = ? "
                    					+ "		  AND id = ?");
            
            preparedStatement.setString(1, certificate.getTitle());
            preparedStatement.setString(2, certificate.getDateCert());
            preparedStatement.setString(3, certificate.getVenue());
            preparedStatement.setString(4, certificate.getDetails());
            preparedStatement.setString(5, certificate.getEmpId());
            preparedStatement.setInt(6, certificate.getId());  
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteCertificate(Certificate certificate) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM certification "     
                    					+ "	 WHERE id = ? "
                    					+ "    AND employee_id = ?");
            
            preparedStatement.setInt(1, certificate.getId());
            preparedStatement.setString(2, certificate.getEmpId()); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    } 
    
    public void addCertificate(Certificate certificate) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO certification" +

                            "(employee_id, title, Date_Cert, venue, Details)" +

                            " VALUES (?, ?, ?, ?, ?)");
            
            preparedStatement.setString(1, certificate.getEmpId());
            preparedStatement.setString(2, certificate.getTitle());
            preparedStatement.setString(3, certificate.getDateCert());
            preparedStatement.setString(4, certificate.getVenue());
            preparedStatement.setString(5, certificate.getDetails());
            preparedStatement.executeUpdate();
          
        } catch (SQLException e) {
            e.printStackTrace();
        }
    } 
    
    public List<Certificate> getCertificateSearchByEmployeeId(String search, String empId) {
        List<Certificate> CertificateSearch = new ArrayList<Certificate>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM certification "
										+ "  	 WHERE (title like ? "
										+ "			OR venue like ? "
										+ "			OR Date_Cert like ? "
										+ "			OR details like ?) "
                    					+ "        AND employee_id = ?");

            preparedStatement.setString(1, "%" + search + "%");
            preparedStatement.setString(2, "%" + search + "%");
            preparedStatement.setString(3, "%" + search + "%");
            preparedStatement.setString(4, "%" + search + "%");
            preparedStatement.setString(5, empId);
            
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {  
            	Certificate certificate = new Certificate();
            	certificate.setDateCert(rs.getString("date_cert"));
            	certificate.setDetails(rs.getString("details"));
            	certificate.setVenue(rs.getString("venue"));
            	certificate.setTitle(rs.getString("title"));
            	
            	CertificateSearch.add(certificate);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return CertificateSearch;
    }
    
    public List<Certificate> getCertificateById(int id, String employeeId) {
        List<Certificate> certificates = new ArrayList<Certificate>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM certification " 
										+ "		 WHERE id = ? "
										+ "		   AND employee_id = ?");

            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, employeeId); 
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {     
            	Certificate certificate = new Certificate();
            	certificate.setId(rs.getInt("ID"));
            	certificate.setDateCert(rs.getString("date_cert"));
            	certificate.setTitle(rs.getString("title"));
            	certificate.setVenue(rs.getString("venue"));
            	certificate.setDetails(rs.getString("details"));
            	certificates.add(certificate);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return certificates;
    }
    
    public List<Certificate> getCertificationByEmployeeId( String empId) {
        List<Certificate> certificates = new ArrayList<Certificate>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM certification " 
								 + "	  WHERE Employee_ID = ? ");

            preparedStatement.setString(1, empId);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {     
            	Certificate certificate = new Certificate();
            	certificate.setId(rs.getInt("ID"));
            	certificate.setDateCert(rs.getString("date_cert"));
            	certificate.setTitle(rs.getString("title"));
            	certificate.setVenue(rs.getString("venue"));
            	certificate.setDetails(rs.getString("details"));
            	certificates.add(certificate);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return certificates;
    }
}
