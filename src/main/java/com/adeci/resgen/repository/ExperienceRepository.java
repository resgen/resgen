package com.adeci.resgen.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.adeci.resgen.config.DatabaseConfiguration;
import com.adeci.resgen.domain.Experience;
import com.adeci.resgen.domain.UserProject;  


public class ExperienceRepository {

    private Connection connection;

    public ExperienceRepository() {
        connection = DatabaseConfiguration.getConnection();
    }

    public void addExperience(Experience experience) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO experience" +

                            "(employee_id,company_name,start_date,end_date,Position)" +

                            " VALUES (?, ?, ?, ?, ?)");

            preparedStatement.setString(1, experience.getEmployeeId());
            preparedStatement.setString(2, experience.getCompanyName());
            preparedStatement.setString(3, experience.getStartDate());
            preparedStatement.setString(4, experience.getEndDate());
            preparedStatement.setString(5, experience.getPosition());
            preparedStatement.executeUpdate();
          
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateProject(UserProject userProject) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE userproject " 
                    					+ "	 SET project_name = ?,"
                    					+ "	     description = ?,"
                    					+ "		 role = ?, "
                    					+ "		 technologies = ? " 
                    					+ "WHERE employee_ID = ? "
                    					+ "  AND company_name = ? "
                    					+ "  AND company_id = ?"
                    					+ "	 AND id = ?");

            preparedStatement.setString(1, userProject.getProjectName());
            preparedStatement.setString(2, userProject.getDescription());
            preparedStatement.setString(3, userProject.getRole());
            preparedStatement.setString(4, userProject.getTechnologies());
            preparedStatement.setString(5, userProject.getEmployeeId());
            preparedStatement.setString(6, userProject.getCompanyName());
            preparedStatement.setInt(7, userProject.getCompanyId());
            preparedStatement.setInt(8, userProject.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteProject(int projectId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM userproject "  
                    					+ "WHERE id = ?");
 
            preparedStatement.setInt(1, projectId); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteExperience(Experience experience) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("DELETE FROM experience "     
                    					+ "	 WHERE id = ? "
                    					+ "    AND employee_id = ?");
            
            preparedStatement.setInt(1, experience.getId());
            preparedStatement.setString(2, experience.getEmployeeId()); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    } 
    
    public void addExperienceProject(UserProject userProject) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO userproject" +

                            "(project_name,description,role,technologies,employee_id,company_name,company_id)" +

                            " VALUES (?, ?, ?, ?, ?, ?, ?)");

            preparedStatement.setString(1, userProject.getProjectName());
            preparedStatement.setString(2, userProject.getDescription());
            preparedStatement.setString(3, userProject.getRole());
            preparedStatement.setString(4, userProject.getTechnologies());
            preparedStatement.setString(5, userProject.getEmployeeId());
            preparedStatement.setString(6, userProject.getCompanyName());
            preparedStatement.setInt(7, userProject.getCompanyId());
            preparedStatement.executeUpdate();
          
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Experience> getExperienceByCompanyId(int companyId, String employeeId) {
        List<Experience> experiences = new ArrayList<Experience>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM experience " 
										+ "		 WHERE id = ? "
										+ "		   AND employee_id = ?");

            preparedStatement.setInt(1, companyId);
            preparedStatement.setString(2, employeeId);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {     
            	Experience experience = new Experience();
                experience.setId(rs.getInt("id"));
                experience.setEmployeeId(rs.getString("employee_id"));
                experience.setCompanyName(rs.getString("company_name"));
                experience.setStartDate(rs.getString("start_date"));
                experience.setEndDate(rs.getString("end_date"));
                experience.setPosition(rs.getString("Position"));
                experiences.add(experience);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return experiences;
    }

    public List<Experience> getExperienceByEmployeeId(String empId) {
        List<Experience> experiences = new ArrayList<Experience>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM experience " 
										+ "		 WHERE Employee_ID = ? "
										+ "	  GROUP BY company_name");

            preparedStatement.setString(1, empId);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {     
            	Experience experience = new Experience();
                experience.setId(rs.getInt("id"));
                experience.setEmployeeId(rs.getString("employee_id"));
                experience.setCompanyName(rs.getString("company_name")); 
                experience.setPosition(rs.getString("Position"));
                experience.setStartDate(rs.getString("start_date"));
                experience.setEndDate(rs.getString("end_date"));
                experiences.add(experience);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return experiences;
    }
    
    public List<UserProject> getProjectById(int companyId, String employeeId) {
        List<UserProject> userProjects = new ArrayList<UserProject>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM userproject "
                    					+ " INNER JOIN experience "
                    					+ "			ON experience.company_name = userproject.company_name" 
										+ "		 WHERE userproject.id = ? "
										+ "		   AND userproject.employee_id = ?"
										+ "	  GROUP BY experience.company_name ");

            preparedStatement.setInt(1, companyId);
            preparedStatement.setString(2, employeeId); 
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {     
            	UserProject userProject = new UserProject();
            	userProject.setId(rs.getInt("userproject.id"));
            	userProject.setProjectName(rs.getString("userproject.project_name"));
            	userProject.setDescription(rs.getString("description"));
            	userProject.setRole(rs.getString("userproject.role"));
            	userProject.setTechnologies(rs.getString("userproject.technologies"));
            	userProject.setEmployeeId(rs.getString("userproject.employee_id"));
            	userProject.setCompanyName(rs.getString("userproject.company_name"));
            	userProject.setCompanyId(rs.getInt("userproject.company_id"));
            	userProjects.add(userProject);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userProjects;
    }
    
    public List<UserProject> getProjectByCompanyId(int companyId, String employeeId) {
        List<UserProject> userProjects = new ArrayList<UserProject>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM userproject "
                    					+ " INNER JOIN experience "
                    					+ "			ON experience.company_name = userproject.company_name" 
										+ "		 WHERE experience.id = ? "
										+ "		   AND experience.employee_id = ?");

            preparedStatement.setInt(1, companyId);
            preparedStatement.setString(2, employeeId); 
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {     
            	UserProject userProject = new UserProject();
            	userProject.setId(rs.getInt("userproject.id"));
            	userProject.setProjectName(rs.getString("userproject.project_name"));
            	userProject.setDescription(rs.getString("description"));
            	userProject.setRole(rs.getString("userproject.role"));
            	userProject.setTechnologies(rs.getString("userproject.technologies"));
            	userProject.setEmployeeId(rs.getString("userproject.employee_id"));
            	userProject.setCompanyName(rs.getString("userproject.company_name"));
            	userProject.setCompanyId(rs.getInt("userproject.company_id"));
            	userProjects.add(userProject);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userProjects;
    }  
    
}