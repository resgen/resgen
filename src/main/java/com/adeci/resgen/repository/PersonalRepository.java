package com.adeci.resgen.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.adeci.resgen.config.DatabaseConfiguration;
import com.adeci.resgen.domain.Personal;  


public class PersonalRepository {

    private Connection connection;

    public PersonalRepository() {
        connection = DatabaseConfiguration.getConnection();
    }

    public void addPersonal(Personal personal) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO personal" +

                            "(lastname,Middlename,firstname,Employee_ID,email)" +

                            " VALUES (?, ?, ?, ?, ?)");

            preparedStatement.setString(1, personal.getLastName());
            preparedStatement.setString(2, personal.getMiddleName());
            preparedStatement.setString(3, personal.getFirstName());
            preparedStatement.setString(4, personal.getEmployeeId()); 
            preparedStatement.setString(5, personal.getEmail());
            preparedStatement.executeUpdate();
          
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePersonal(Personal personal) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE personal "    
                    					+ "	 SET lastname = ?,"
                    					+ "		 Middlename = ?,"
                    					+ "		 home_add = ?,"
                    					+ "		 contact = ?,"
                    					+ "		 email = ?,"
                    					+ "		 birthdate = ?,"
                    					+ "		 civil_status = ?," 
                    					+ "		 overview = ?,"
                    					+ "		 firstname = ?,"
                    					+ "		 User_role = ?,"
                    					+ "		 Level = ? " 
                    					+ "WHERE Employee_ID = ? ");
            
            preparedStatement.setString(1, personal.getLastName());
            preparedStatement.setString(2, personal.getMiddleName());
            preparedStatement.setString(3, personal.getAddress());
            preparedStatement.setString(4, personal.getContact());
            preparedStatement.setString(5, personal.getEmail());
            preparedStatement.setString(6, personal.getBirthDate());
            preparedStatement.setString(7, personal.getCivilStatus()); 
            preparedStatement.setString(8, personal.getOverView());
            preparedStatement.setString(9, personal.getFirstName());
            preparedStatement.setString(10, personal.getUserRole());
            preparedStatement.setString(11, personal.getLevel());
            preparedStatement.setString(12, personal.getEmployeeId()); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updatePersonalPicture(Personal personal) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE personal "    
                    					+ "	 SET picture = ? " 
                    					+ "WHERE Employee_ID = ? ");
             
            preparedStatement.setString(1, personal.getPicture()); 
            preparedStatement.setString(2, personal.getEmployeeId()); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Personal> getAllPersonal() {
        List<Personal> personals = new ArrayList<Personal>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM personal");
            while (rs.next()) {
                Personal personal = new Personal();
                personal.setID(rs.getInt("ID"));
                personal.setFirstName(rs.getString("firstname"));
                personal.setLastName(rs.getString("lastname"));
                personal.setMiddleName(rs.getString("Middlename"));
                personal.setAddress(rs.getString("home_add"));
                personal.setContact(rs.getString("contact"));
                personal.setEmail(rs.getString("email"));
                personal.setBirthDate(rs.getString("birthdate"));
                personal.setCivilStatus(rs.getString("civil_status"));
                personal.setPicture(rs.getString("picture"));
                personal.setOverView(rs.getString("overview"));
                personal.setEmployeeId(rs.getString("Employee_ID"));
                personal.setUserRole(rs.getString("User_role"));
                personal.setLevel(rs.getString("Level"));
                personals.add(personal);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return personals;
    }  
    
    public List<Personal> getPersonalByEmployeeId(String empId) {
    	Personal personal = new Personal();
        List<Personal> personals = new ArrayList<Personal>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM personal " 
										+ "		 WHERE Employee_ID = ? ");

            preparedStatement.setString(1, empId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {   
                personal.setID(rs.getInt("ID"));
                personal.setFirstName(rs.getString("firstname"));
                personal.setLastName(rs.getString("lastname"));
                personal.setMiddleName(rs.getString("Middlename"));
                personal.setAddress(rs.getString("home_add"));
                personal.setContact(rs.getString("contact"));
                personal.setEmail(rs.getString("email"));
                personal.setBirthDate(rs.getString("birthdate"));
                personal.setCivilStatus(rs.getString("civil_status"));
                personal.setPicture(rs.getString("picture"));
                personal.setOverView(rs.getString("overview"));
                personal.setEmployeeId(rs.getString("Employee_ID"));
                personal.setUserRole(rs.getString("User_role"));
                personal.setLevel(rs.getString("Level"));
                personals.add(personal);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personals;
    }
    
}