package com.adeci.resgen.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.adeci.resgen.config.DatabaseConfiguration;
import com.adeci.resgen.domain.PersonalAndUser;
import com.adeci.resgen.domain.User;
import com.adeci.resgen.security.algorithm.Authentication;  


public class UserRepository {

    private Connection connection;

    public UserRepository() {
        connection = DatabaseConfiguration.getConnection();
    }

    public void addUser(User user) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO users" +

                            "(Employee_ID,Password,Usertype,Email,Active)" +

                            " VALUES (?, ?, ?, ?, ?)");

            preparedStatement.setString(1, user.getEmployeeId());
            preparedStatement.setString(2, user.getPassWord());
            preparedStatement.setString(3, user.getUserType());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getActive());
            preparedStatement.executeUpdate();
          
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM users");
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("ID"));
                user.setEmployeeId(rs.getString("Employee_ID"));
                user.setUserType(rs.getString("Usertype"));
                user.setEmail(rs.getString("Email"));
                user.setActive(rs.getString("Active"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    } 
    
    public List<PersonalAndUser> getAllUserMaintenance() {
        List<PersonalAndUser> personalAndUsers = new ArrayList<PersonalAndUser>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM users "
            										+ " INNER JOIN personal"
            										+ "			ON personal.Employee_ID = users.Employee_ID "
            										+ "	  GROUP BY users.Employee_ID ");
            while (rs.next()) {
            	PersonalAndUser personalAndUser = new PersonalAndUser();
            	personalAndUser.setID(rs.getInt("ID"));
            	personalAndUser.setFirstName(rs.getString("firstname"));
            	personalAndUser.setLastName(rs.getString("lastname"));
            	personalAndUser.setMiddleName(rs.getString("personal.Middlename"));
            	personalAndUser.setAddress(rs.getString("home_add"));
            	personalAndUser.setContact(rs.getString("contact"));
            	personalAndUser.setEmail(rs.getString("users.Email"));
                personalAndUser.setBirthDate(rs.getString("birthdate"));
                personalAndUser.setCivilStatus(rs.getString("civil_status"));
                personalAndUser.setPicture(rs.getString("picture"));
                personalAndUser.setOverView(rs.getString("overview"));
                personalAndUser.setEmployeeId(rs.getString("Employee_ID"));
                personalAndUser.setUserRole(rs.getString("User_role"));
                personalAndUser.setLevel(rs.getString("Level"));
                personalAndUser.setUserType(rs.getString("Usertype"));
                personalAndUser.setActive(rs.getString("Active"));
                personalAndUsers.add(personalAndUser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return personalAndUsers;
    }
     
    public List<PersonalAndUser> getUserMaintenanceSearchByEmployeeId(String id) {
        List<PersonalAndUser> personalAndUsers = new ArrayList<PersonalAndUser>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM users "
										+ " INNER JOIN personal"
										+ "			ON personal.Employee_ID = users.Employee_ID "
										+ "  	 WHERE users.Employee_ID like ? "
										+ "			OR personal.firstname like ? "
										+ "			OR personal.lastname like ? "
										+ "			OR personal.Middlename like ? "
										+ "			OR Usertype like ? "
										+ "			OR users.Email like ? "
										+ "			OR Active like ? "
										+ "	  GROUP BY users.Employee_ID ");

            preparedStatement.setString(1, "%" + id + "%"); 
            preparedStatement.setString(2, "%" + id + "%"); 
            preparedStatement.setString(3, "%" + id + "%"); 
            preparedStatement.setString(4, "%" + id + "%"); 
            preparedStatement.setString(5, "%" + id + "%"); 
            preparedStatement.setString(6, "%" + id + "%"); 
            preparedStatement.setString(7, "%" + id + "%"); 
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {  
            	PersonalAndUser personalAndUser = new PersonalAndUser();
            	personalAndUser.setID(rs.getInt("ID"));
            	personalAndUser.setFirstName(rs.getString("firstname"));
            	personalAndUser.setLastName(rs.getString("lastname"));
            	personalAndUser.setMiddleName(rs.getString("personal.Middlename"));
            	personalAndUser.setAddress(rs.getString("home_add"));
            	personalAndUser.setContact(rs.getString("contact"));
            	personalAndUser.setEmail(rs.getString("users.Email"));
                personalAndUser.setBirthDate(rs.getString("birthdate"));
                personalAndUser.setCivilStatus(rs.getString("civil_status"));
                personalAndUser.setPicture(rs.getString("picture"));
                personalAndUser.setOverView(rs.getString("overview"));
                personalAndUser.setEmployeeId(rs.getString("Employee_ID"));
                personalAndUser.setUserRole(rs.getString("User_role"));
                personalAndUser.setLevel(rs.getString("Level"));
                personalAndUser.setUserType(rs.getString("Usertype"));
                personalAndUser.setActive(rs.getString("Active"));
                personalAndUsers.add(personalAndUser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personalAndUsers;
    }
    
    public List<PersonalAndUser> getUserByEmployeeId(String empId) {
        List<PersonalAndUser> personalAndUsers = new ArrayList<PersonalAndUser>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM users "
										+ " INNER JOIN personal"
										+ "			ON personal.Employee_ID = users.Employee_ID "
										+ "  	 WHERE users.Employee_ID = ? ");

            preparedStatement.setString(1,empId);  
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {  
            	PersonalAndUser personalAndUser = new PersonalAndUser();
            	personalAndUser.setID(rs.getInt("ID"));
            	personalAndUser.setFirstName(rs.getString("firstname"));
            	personalAndUser.setLastName(rs.getString("lastname"));
            	personalAndUser.setMiddleName(rs.getString("personal.Middlename"));
            	personalAndUser.setAddress(rs.getString("home_add"));
            	personalAndUser.setContact(rs.getString("contact"));
            	personalAndUser.setEmail(rs.getString("users.Email"));
                personalAndUser.setBirthDate(rs.getString("birthdate"));
                personalAndUser.setCivilStatus(rs.getString("civil_status"));
                personalAndUser.setPicture(rs.getString("picture"));
                personalAndUser.setOverView(rs.getString("overview"));
                personalAndUser.setEmployeeId(rs.getString("Employee_ID"));
                personalAndUser.setUserRole(rs.getString("User_role"));
                personalAndUser.setLevel(rs.getString("Level"));
                personalAndUser.setUserType(rs.getString("Usertype"));
                personalAndUser.setActive(rs.getString("Active"));
                personalAndUsers.add(personalAndUser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personalAndUsers;
    }
    
    public List<PersonalAndUser> getUserMaintenanceById(int id) {
    	PersonalAndUser personalAndUser = new PersonalAndUser();
        List<PersonalAndUser> personalAndUsers = new ArrayList<PersonalAndUser>();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("	 SELECT * FROM users "
										+ " INNER JOIN personal"
										+ "			ON personal.Employee_ID = users.Employee_ID "
										+ "  	 WHERE users.ID = ? "
										+ "		   AND users.Active = 'Active'");

            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {  
            	personalAndUser.setID(rs.getInt("ID"));
            	personalAndUser.setFirstName(rs.getString("firstname"));
            	personalAndUser.setLastName(rs.getString("lastname"));
            	personalAndUser.setMiddleName(rs.getString("personal.Middlename"));
            	personalAndUser.setAddress(rs.getString("home_add"));
            	personalAndUser.setContact(rs.getString("contact"));
            	personalAndUser.setEmail(rs.getString("users.Email"));
                personalAndUser.setBirthDate(rs.getString("birthdate"));
                personalAndUser.setCivilStatus(rs.getString("civil_status"));
                personalAndUser.setPicture(rs.getString("picture"));
                personalAndUser.setOverView(rs.getString("overview"));
                personalAndUser.setEmployeeId(rs.getString("Employee_ID"));
                personalAndUser.setUserRole(rs.getString("User_role"));
                personalAndUser.setLevel(rs.getString("Level"));
                personalAndUser.setUserType(rs.getString("Usertype"));
                personalAndUser.setActive(rs.getString("Active"));
                personalAndUsers.add(personalAndUser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personalAndUsers;
    } 
    
    public void updateUserMaintenance(PersonalAndUser personalAndUser) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE users "
                    					+ " 	JOIN personal on users.Employee_ID = personal.Employee_ID"
                    					+ "	 	 SET users.Employee_ID = ?, "
                    					+ "		 	 personal.Employee_ID = ?, "
                    					+ "		 	 personal.firstname = ?, "
                    					+ "		 	 personal.lastname = ?, "
                    					+ "		 	 personal.Middlename = ?, " 
                    					+ "		 	 users.Email = ?, "
                    					+ "		 	 personal.email = ?,"
                    					+ "		 	 users.Active = ?, "
                    					+ "			 users.Usertype = ? "
                    					+ "	   WHERE users.Employee_ID = ? ");

            preparedStatement.setString(1, personalAndUser.getEmployeeId());
            preparedStatement.setString(2, personalAndUser.getEmployeeId());
            preparedStatement.setString(3, personalAndUser.getFirstName());
            preparedStatement.setString(4, personalAndUser.getLastName());
            preparedStatement.setString(5, personalAndUser.getMiddleName());
            preparedStatement.setString(6, personalAndUser.getEmail());
            preparedStatement.setString(7, personalAndUser.getEmail());
            preparedStatement.setString(8, personalAndUser.getActive());
            preparedStatement.setString(9, personalAndUser.getUserType());
            preparedStatement.setString(10, personalAndUser.getEmployeeId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updateUserPassword(String empId,String password) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE users " 
                    					+ "		 SET Password = ? " 
                    					+ "	   WHERE Employee_ID = ? ");

            preparedStatement.setString(1, password);
            preparedStatement.setString(2, empId); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updateUserMaintenanceStatus(String empId,String status) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE users "
                    					+ " 	JOIN personal on users.Employee_ID = personal.Employee_ID"
                    					+ "		 SET users.Active = ? " 
                    					+ "    WHERE users.Employee_ID = ? ");

            preparedStatement.setString(1, status);
            preparedStatement.setString(2, empId); 
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public boolean isDuplicateEmployeeId(String employeeId) { 
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * " 
                            			+ "FROM users WHERE Employee_ID=?");

            preparedStatement.setString(1, employeeId);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) { 
                return true; 
            }  
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
    
    public boolean isUserEmployeeActive(String empId) { 
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * " 
                    					+ "		FROM users WHERE Employee_ID = ?"
                    					+ "	 	 AND Active = 'Active' ");
 
            preparedStatement.setString(1, empId);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) { 
                return true; 
            }  
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
    
    public boolean isUserMaintenanceIdActive(int userId) { 
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * " 
                    					+ "		FROM users WHERE ID = ?"
                    					+ "	 	 AND Active = 'Active' ");
 
            preparedStatement.setInt(1, userId);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) { 
                return true; 
            }  
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
    
    public boolean getUserAuthentication(String employeeId,String passWord) {
        User user = new User();
        Authentication authentication = new Authentication();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("SELECT * "+

                            "FROM users WHERE Employee_ID=?");

            preparedStatement.setString(1, employeeId);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) { 
                user.setPassWord(rs.getString("Password")); 
            } 
            try{
                //Password Decrypt then Authenticate
                if(authentication.checkUserPassword(passWord, user.getPassWord())){ 
                    return true;
                }
                else{ 
                    return false;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
}