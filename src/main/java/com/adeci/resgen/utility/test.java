package com.adeci.resgen.utility;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.pdf.draw.VerticalPositionMark; 

public class test { 
	public static PdfPCell getCell(String text, int alignment) {
	    PdfPCell cell = new PdfPCell(new Phrase(text));
	    cell.setPadding(0);
	    cell.setHorizontalAlignment(alignment);
	    cell.setBorder(PdfPCell.NO_BORDER);
	    return cell;
	} 
	 
	public static void main(String[] args) throws FileNotFoundException, DocumentException{ 
		//UserRepository userRepository = new UserRepository();
		String bulletPoint = "\u2022";
		Document document = new Document(PageSize.A4); 
	    PdfWriter.getInstance(document, new FileOutputStream("Generated Resume.pdf")); 
	    Font font27 = FontFactory.getFont(FontFactory.TIMES_BOLD,27,BaseColor.BLACK);
	    Font font15 = FontFactory.getFont(FontFactory.TIMES,15,BaseColor.BLACK); 
	    Font fontWhite = FontFactory.getFont(FontFactory.COURIER_BOLDOBLIQUE,15,BaseColor.WHITE); 
	    Chunk glue = new Chunk(new VerticalPositionMark()); 
	    
	    //Opens the document
	    document.open(); 
	    
	    document.add(new Paragraph("Siegfrid Saint Llyr Medrana",font27));   
	    document.add(new Paragraph(" "));        
	    document.add(new LineSeparator());
	    document.add(new Paragraph("SYSTEMS DEVELOPER",font15));   
	    
	    //LEVEL AND YEAR EXPERIENCE
	    Paragraph LevelAndYears = new Paragraph();   
	    Chunk cLevel = new Chunk(" LEVEL ",fontWhite);
	    Chunk cYears = new Chunk(" YEARS EXPERIENCE ",fontWhite);
	    cLevel.setBackground(BaseColor.BLACK);
	    cYears.setBackground(BaseColor.BLACK);
	    LevelAndYears.add(cLevel);
	    LevelAndYears.add(" Lead Developer");
	    LevelAndYears.add(new Chunk(glue));
	    LevelAndYears.add(cYears);
	    LevelAndYears.add(" 9999+"); 
	    document.add(LevelAndYears);
	    document.add(new Paragraph(" ")); 
	    document.add(new LineSeparator());
	    
	    // OVER VIEW
	    Paragraph overView = new Paragraph();
	    Chunk cOverView = new Chunk(" OVERVIEW ", fontWhite); 
	    cOverView.setBackground(BaseColor.BLACK);
	    document.add(cOverView);
	    overView.setAlignment(Element.ALIGN_CENTER);
	    overView.add("Systems Developer with more than 6 years of technical expertise "
	    				+ " in developing systems and applications.");
	    document.add(overView); 
	    document.add(new Paragraph(" "));
	    
	    // TABLE TITLE EXPERIENCE AND SKILLS
	    PdfPTable tableHeadExpAndSkills = new PdfPTable(2);
	    PdfPCell cellHeadExperience;
	    PdfPCell cellHeadSkill;
	    tableHeadExpAndSkills.setWidthPercentage(100); 
	    cellHeadExperience = new PdfPCell(new Phrase("EXPERIENCE",fontWhite)); 
	    cellHeadExperience.setBackgroundColor(BaseColor.BLACK);
	    cellHeadExperience.setBorder(PdfPCell.NO_BORDER);  
	    cellHeadSkill = new PdfPCell(new Phrase("SKILLS",fontWhite)); 
	    cellHeadSkill.setBackgroundColor(BaseColor.BLACK);
	    cellHeadSkill.setBorder(PdfPCell.NO_BORDER);  
	    tableHeadExpAndSkills.addCell(cellHeadExperience);
	    tableHeadExpAndSkills.addCell(cellHeadSkill);
	    document.add(tableHeadExpAndSkills);
	    document.add(new Paragraph(" "));
	    
	    // TABLE DETAILS EXPERIENCE AND SKILLS
	    PdfPTable tableExperienceAndSkills = new PdfPTable(2);
	    PdfPCell cellExperienceAndSkills;
	    tableExperienceAndSkills.setWidthPercentage(100); 
	    for(int i = 0;i<5;i++){    
		    cellExperienceAndSkills = new PdfPCell(new Phrase(bulletPoint +" Experiences"));
	    	cellExperienceAndSkills.setBorder(PdfPCell.NO_BORDER); 
	    	cellExperienceAndSkills.enableBorderSide(Rectangle.RIGHT);
	    	tableExperienceAndSkills.addCell(cellExperienceAndSkills); 
		    tableExperienceAndSkills.addCell(getCell("  Skills ", PdfPCell.ALIGN_LEFT));
	    }
	    document.add(tableExperienceAndSkills);
	    
	    //New Document Page 
	    document.newPage();
	    document.add(new Phrase("Hey"));
	    
	    //Close the document
	    document.close();
	} 
	
}
