package com.adeci.resgen.utility.regex;

public class PasswordRegex {
	public static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})";
	//#   must contains one digit from 0-9
	//#   must contains one Lower case characters
	//#   must contains one Upper case characters
	//#   length at least 8 characters and maximum of 20	
	
	public boolean checkPasswordPattern(String passWord){ 
		Boolean validPassword = passWord.matches(PASSWORD_PATTERN);    
		
		if(validPassword){
			return true;
		} else {
			return false;
		}
	}
}
