package com.adeci.resgen.utility.regex;

public class EmailRegex {
	public static final String EMAIL_PATTERN = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

	public boolean checkEmailPattern(String email){ 
		Boolean validEmail = email.matches(EMAIL_PATTERN);    
		
		if(validEmail){
			return true;
		} else {
			return false;
		}
	}
}
