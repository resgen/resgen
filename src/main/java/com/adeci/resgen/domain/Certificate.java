package com.adeci.resgen.domain;

public class Certificate {
	private int id;
	private String empId;
	private String title;
	private String dateCert;
	private String venue;
	private String details;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getDateCert() {
		return dateCert;
	}
	public void setDateCert(String dateCert) {
		this.dateCert = dateCert;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
