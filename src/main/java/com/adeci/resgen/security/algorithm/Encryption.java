package com.adeci.resgen.security.algorithm;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

public class Encryption {
	public String encryptPassword(String passwordInput) {
        ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
        passwordEncryptor.setAlgorithm("SHA-256");
        passwordEncryptor.setPlainDigest(true);
        String encryptedPassword = passwordEncryptor.encryptPassword(passwordInput); 
        return encryptedPassword;
    }
}
