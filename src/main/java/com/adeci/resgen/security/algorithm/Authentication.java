package com.adeci.resgen.security.algorithm;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

public class Authentication {
	public boolean checkUserPassword(String passwordInput,String passwordMatch) {
		ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
		passwordEncryptor.setAlgorithm("SHA-256");
		passwordEncryptor.setPlainDigest(true);

		if (passwordEncryptor.checkPassword(passwordInput, passwordMatch)) { 
			return true;
		} else { 
			return false;
		}
		
	}
}
