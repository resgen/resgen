package com.adeci.resgen.security;

import static com.adeci.resgen.controller.util.VariableConstants.CONTEXT;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/404")
public class ErrorRedirect extends HttpServlet {
	private static final long serialVersionUID = 1L; 
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(); 
		CONTEXT = request.getContextPath();
		RequestDispatcher dispatcher; 
		boolean isLoggedIn = session != null && session.getAttribute("SessionEmployeeId") != null;
		
		if(!isLoggedIn){ 
			dispatcher = request.getRequestDispatcher("WEB-INF/main/util/ErrorPage.jsp");
		} else {
			dispatcher = request.getRequestDispatcher("WEB-INF/main/util/ErrorPageLogin.jsp");
		}
		dispatcher.forward(request, response);
	} 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/main/util/ErrorPage.jsp");
		dispatcher.forward(request, response);
	}

}