package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.CONTEXT;
import static com.adeci.resgen.controller.util.VariableConstants.HOME_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.HOME_SERVLET;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Home"})
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher; 
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		dispatcher = request.getRequestDispatcher(HOME_PAGE);
        dispatcher.forward(request, response);
        
	} 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CONTEXT = request.getContextPath();
		response.sendRedirect(CONTEXT + HOME_SERVLET);
		
	}

}
