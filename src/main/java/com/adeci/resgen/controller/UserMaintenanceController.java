package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.CONTEXT;
import static com.adeci.resgen.controller.util.VariableConstants.ERROR_404_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.USER_MAINTENANCE_ADD_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.USER_MAINTENANCE_CREATE_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.USER_MAINTENANCE_EDIT_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.USER_MAINTENANCE_EDIT_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.USER_MAINTENANCE_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.USER_MAINTENANCE_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.USER_MAINTENANCE_STATUS_SERVLET;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.adeci.resgen.domain.Personal;
import com.adeci.resgen.domain.PersonalAndUser;
import com.adeci.resgen.domain.User;
import com.adeci.resgen.repository.PersonalRepository;
import com.adeci.resgen.repository.UserRepository;
import com.adeci.resgen.security.algorithm.Encryption;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/UserMaintenance",
		"/UserMaintenance/User",
		"/UserMaintenance/User/Status",
		"/UserMaintenance/Create"})
public class UserMaintenanceController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher;
	private UserRepository userRepository = new UserRepository();
	private PersonalRepository personalRepository = new PersonalRepository();
	private User user = new User();
	private Personal personal = new Personal(); 
	private Encryption encryption = new Encryption(); 

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String URL = request.getServletPath();
		CONTEXT = request.getContextPath();
		
		if(URL.equals(USER_MAINTENANCE_SERVLET)){
			String search = request.getParameter("userSearchInput"); 
			
			if(search == null){
				request.setAttribute("users",  userRepository.getAllUserMaintenance());	 
				 
			} else {
				request.setAttribute("users", userRepository.getUserMaintenanceSearchByEmployeeId(search));
				
			}
			
			//Remove Search parameter in URL
			if(search == ""){
				response.sendRedirect(CONTEXT + USER_MAINTENANCE_SERVLET);
				
			} else { 
				dispatcher = request.getRequestDispatcher(USER_MAINTENANCE_PAGE);
				dispatcher.forward(request, response);
				
			}
			
		} else if(URL.equals(USER_MAINTENANCE_CREATE_SERVLET)){
			dispatcher = request.getRequestDispatcher(USER_MAINTENANCE_ADD_PAGE);
			dispatcher.forward(request, response); 
			
		} else if(URL.equals(USER_MAINTENANCE_STATUS_SERVLET)){
			String empId = request.getParameter("id");
			String status = request.getParameter("status").equals("Active") ? "Inactive" : "Active";
		  
			userRepository.updateUserMaintenanceStatus(empId,status);
			response.sendRedirect(CONTEXT + USER_MAINTENANCE_SERVLET); 
			
		} else if(URL.equals(USER_MAINTENANCE_EDIT_SERVLET)){
			int id = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("userInfo", userRepository.getUserMaintenanceById(id)); 
			boolean isUserActive = userRepository.isUserMaintenanceIdActive(id); 
			
			if(isUserActive){
				dispatcher = request.getRequestDispatcher(USER_MAINTENANCE_EDIT_PAGE);
				dispatcher.forward(request, response); 
			} else {
				response.sendRedirect(CONTEXT + ERROR_404_SERVLET);
			} 
			
		} else {
			response.sendRedirect(CONTEXT + USER_MAINTENANCE_SERVLET);
		}
	} 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		String URL = request.getServletPath();
		CONTEXT = request.getContextPath(); 
		boolean isDuplicateUser = userRepository.isDuplicateEmployeeId(request.getParameter("employeeId"));
		
		if(URL.equals(USER_MAINTENANCE_EDIT_SERVLET)){
			int userId = Integer.parseInt(request.getParameter("userId"));
			String status = request.getParameter("userStatus") == null ? "Inactive" : "Active";
			PersonalAndUser personalAndUser = new PersonalAndUser();
			personalAndUser.setID(userId);
			personalAndUser.setEmployeeId(request.getParameter("employeeId"));
			personalAndUser.setFirstName(request.getParameter("firstName"));
			personalAndUser.setLastName(request.getParameter("lastName"));
			personalAndUser.setMiddleName(request.getParameter("middleName"));
			personalAndUser.setEmail(request.getParameter("email"));
			personalAndUser.setActive(status);
			personalAndUser.setUserType(request.getParameter("userType"));
			userRepository.updateUserMaintenance(personalAndUser);
			response.sendRedirect(status == "Active" ? CONTEXT + USER_MAINTENANCE_EDIT_SERVLET+"?id="+userId : CONTEXT + USER_MAINTENANCE_SERVLET);
			
		} else if(URL.equals(USER_MAINTENANCE_CREATE_SERVLET)){
			if(!isDuplicateUser){
				user.setPassWord(encryption.encryptPassword(request.getParameter("employeeId")));
				user.setEmployeeId(request.getParameter("employeeId"));
				user.setActive("Inactive");
				user.setUserType(request.getParameter("userType"));
				user.setEmail(request.getParameter("email")); 
				personal.setFirstName(request.getParameter("firstName"));
				personal.setMiddleName(request.getParameter("middleName"));
				personal.setLastName(request.getParameter("lastName"));
				personal.setEmail(request.getParameter("email"));
				personal.setEmployeeId(request.getParameter("employeeId")); 
				userRepository.addUser(user);
				personalRepository.addPersonal(personal); 
				
			}  
			response.sendRedirect(CONTEXT + USER_MAINTENANCE_SERVLET);
			
		} else {
			response.sendRedirect(CONTEXT + USER_MAINTENANCE_SERVLET);
			
		} 
	} 
}
