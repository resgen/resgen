package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adeci.resgen.domain.Education;
import com.adeci.resgen.repository.EducationRepository;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Education",
							"/Education/Create",
							"/Education/Delete",
							"/Education/Edit"})
public class EducationController extends HttpServlet {
	private static final long serialVersionUID = 1L; 
	private RequestDispatcher dispatcher; 
	private EducationRepository educationRepository = new EducationRepository();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(); 
		CONTEXT = request.getContextPath(); 
		
		String URL = request.getServletPath();
		String empId =  (String) session.getAttribute("SessionEmployeeId");
		
		if(URL.equals(EDUCATION_SERVLET)){
			String search = request.getParameter("educationSearchInput"); 
			
			if(search == null){
				request.setAttribute("educationsInfo", educationRepository.getEducationByEmployeeId(empId));
				
			} else {
				request.setAttribute("educationsInfo", educationRepository.getEducationSearchByEmployeeId(search, empId));
				
			}
			
			if(search == ""){
				response.sendRedirect(CONTEXT + EDUCATION_SERVLET);
				
			} else { 
				dispatcher = request.getRequestDispatcher(EDUCATION_PAGE);
				dispatcher.forward(request, response);
				
			}
			
		} else if(URL.equals(EDUCATION_CREATE_SERVLET)){
			
			//Check for errors
			if(session.getAttribute("EducationNullError") != null){
				request.setAttribute("EducationNullError", session.getAttribute("EducationNullError"));
				session.removeAttribute("EducationNullError");
				
			}
			
			if(session.getAttribute("SuccessEducationCreate") != null){
				request.setAttribute("SuccessEducationCreate", session.getAttribute("SuccessEducationCreate"));
				session.removeAttribute("SuccessEducationCreate");
				
			}
			
			dispatcher = request.getRequestDispatcher(EDUCATION_ADD_PAGE);
			dispatcher.forward(request, response);
			
		} else if(URL.equals(EDUCATION_DELETE_SERVLET)){
			int id = Integer.parseInt(request.getParameter("id"));
			
			educationRepository.deleteEducation(id);
			response.sendRedirect(CONTEXT + EDUCATION_SERVLET);
			
		} else if(URL.equals(EDUCATION_EDIT_SERVLET)){ 
			int id = Integer.parseInt(request.getParameter("id"));
			boolean isEducationFromEmployee = educationRepository.isEducationFromEmployee(empId,id);
			
			//Check for errors
			if(session.getAttribute("EducationNullError") != null){
				request.setAttribute("EducationNullError", session.getAttribute("EducationNullError"));
				session.removeAttribute("EducationNullError");
				
			}
			
			if(session.getAttribute("SuccessEducationUpdate") != null){
				request.setAttribute("SuccessEducationUpdate", session.getAttribute("SuccessEducationUpdate"));
				session.removeAttribute("SuccessEducationUpdate");
				
			}
			
			if(isEducationFromEmployee){
				request.setAttribute("educationInfo", educationRepository.getEducationById(id, empId));
				dispatcher = request.getRequestDispatcher(EDUCATION_EDIT_PAGE);
				dispatcher.forward(request, response);
				
			} else {
				response.sendRedirect(CONTEXT + ERROR_404_SERVLET);
				
			}
			
		} else {
			response.sendRedirect(CONTEXT + EDUCATION_SERVLET);
			
		} 
         
	} 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(); 
		CONTEXT = request.getContextPath(); 
		 
		String URL = request.getServletPath();
		String empId =  (String) session.getAttribute("SessionEmployeeId"); 
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String schoolName = request.getParameter("schoolName");
		String program = request.getParameter("program");
		String achievement = request.getParameter("achievement");
		boolean isFieldNull = empId == null || startDate == null || endDate == null || schoolName == null ||
							  program == null || achievement == null || empId == "" || startDate == "" || 
							  endDate == "" || schoolName == "" || program == "" || achievement == "";
		
		Education education = new Education();
		EducationRepository educationRepository = new EducationRepository();
		
		if(URL.equals(EDUCATION_CREATE_SERVLET)){
			if(isFieldNull){
				session.setAttribute("EducationNullError", "Please fill up all the fields! ");
				
			}
			
			if(!isFieldNull){
				education.setEmployeeId(empId);
				education.setStartDate(startDate);
				education.setEndDate(endDate);
				education.setSchoolName(schoolName);
				education.setProgram(program);
				education.setAchievement(achievement);
				educationRepository.addEducation(education);
				session.setAttribute("SuccessEducationCreate", "Successfully created! ");
				
			}
			
			response.sendRedirect(CONTEXT + EDUCATION_CREATE_SERVLET);
			
		} else if(URL.equals(EDUCATION_EDIT_SERVLET)){		
			int educationId = Integer.parseInt(request.getParameter("educationId"));
			boolean isEducationFromEmployee = educationRepository.isEducationFromEmployee(empId,educationId);
			
			if(isFieldNull){
				session.setAttribute("EducationNullError", "Please fill up all the fields! ");
				
			}
			
			if(!isFieldNull){ 
				education.setId(educationId);
				education.setEmployeeId(empId);		 
				education.setStartDate(startDate);			 
				education.setEndDate(endDate);			 
				education.setSchoolName(schoolName);		 
				education.setProgram(program);		 
				education.setAchievement(achievement); 
				educationRepository.updateEducation(education);			
				session.setAttribute("SuccessEducationUpdate", "Successfully updated! ");
		
			}  
			
			if(isEducationFromEmployee){
				response.sendRedirect(CONTEXT + EDUCATION_EDIT_SERVLET + "?id=" + educationId);
				
			} 
			 
		}

	}
}
