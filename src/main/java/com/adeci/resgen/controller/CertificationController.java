package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adeci.resgen.domain.Certificate;
import com.adeci.resgen.repository.CertificationRepository;



/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Certification"
						, "/Certification/Create"
						, "/Certification/Delete"
						, "/Certification/Edit"})
public class CertificationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher; 
	Certificate certificate = new Certificate();
	private CertificationRepository certificationRepository = new CertificationRepository();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		CONTEXT = request.getContextPath(); 
		
		String URL = request.getServletPath();
		String empId =  (String) session.getAttribute("SessionEmployeeId");		
		
		if(URL.equals(CERTIFICATE_SERVLET)){
	        String search = request.getParameter("certificateSearchInput"); 
			
			if(search == null){
				request.setAttribute("certificationInfo", certificationRepository.getCertificationByEmployeeId(empId));	 
				 
			} else {
				request.setAttribute("certificationInfo", certificationRepository.getCertificateSearchByEmployeeId(search, empId));
				
			}
			
			if(search == ""){
				response.sendRedirect(CONTEXT + CERTIFICATE_SERVLET);
				
			} else { 
				dispatcher = request.getRequestDispatcher(CERTIFICATION_PAGE);
				dispatcher.forward(request, response);
				
			}
	        
		} else if(URL.equals(CERTIFICATE_CREATE_SERVLET)){
			
			//Check for errors
			if(session.getAttribute("CertificationNullError") != null){
				request.setAttribute("CertificationNullError", session.getAttribute("CertificationNullError"));
				session.removeAttribute("CertificationNullError");
				
			}
			
			if(session.getAttribute("SuccessCertificationCreate") != null){
				request.setAttribute("SuccessCertificationCreate", session.getAttribute("SuccessCertificationCreate"));
				session.removeAttribute("SuccessCertificationCreate");
				
			}
			
			
			dispatcher = request.getRequestDispatcher(CERTIFICATION_ADD_PAGE);
	        dispatcher.forward(request, response);	
	        
		} else if(URL.equals(CERTIFICATE_DELETE_SERVLET)){
			certificate.setEmpId(empId);
			certificate.setId(Integer.parseInt(request.getParameter("id")));
			certificationRepository.deleteCertificate(certificate);
			response.sendRedirect(CONTEXT + CERTIFICATE_SERVLET);
		
		} else if(URL.equals(CERTIFICATE_EDIT_SERVLET)){
			
			//Check for errors
			if(session.getAttribute("CertificationNullError") != null){
				request.setAttribute("CertificationNullError", session.getAttribute("CertificationNullError"));
				session.removeAttribute("CertificationNullError");
				
			}
			
			if(session.getAttribute("SuccessCertificationUpdate") != null){
				request.setAttribute("SuccessCertificationUpdate", session.getAttribute("SuccessCertificationUpdate"));
				session.removeAttribute("SuccessCertificationUpdate");
				
			}
			
			int id = Integer.parseInt(request.getParameter("id")); 
			request.setAttribute("certificationInfo", certificationRepository.getCertificateById(id, empId));
			dispatcher = request.getRequestDispatcher(CERTIFICATION_EDIT_PAGE);
			dispatcher.forward(request, response);
			
		} else {
			response.sendRedirect(CONTEXT + CERTIFICATE_SERVLET);
			
		}
	} 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(); 
		CONTEXT = request.getContextPath(); 
		
		String URL = request.getServletPath();
		String empId =  (String) session.getAttribute("SessionEmployeeId");
		String title = request.getParameter("title");
		String date_cert = request.getParameter("Date_Cert");
		String venue = request.getParameter("Venue");
		String details = request.getParameter("Details");
		boolean isFieldNull = empId == null || title == null || date_cert == null || venue == null ||
							  details == null || empId == "" || title == "" || date_cert == "" || 
							  venue == "" || details == "";
		
		Certificate certificate = new Certificate();
		CertificationRepository certificationRepository = new CertificationRepository();
		
		if(URL.equals(CERTIFICATE_CREATE_SERVLET)){
			if(isFieldNull){
				session.setAttribute("CertificationNullError", "Please fill up all the fields! ");
			}
			
			if(!isFieldNull){
				certificate.setEmpId(empId);
				certificate.setTitle(title);
				certificate.setDateCert(date_cert);
				certificate.setVenue(venue);
				certificate.setDetails(details);
				certificationRepository.addCertificate(certificate);
				session.setAttribute("SuccessCertificationCreate", "Successfully created! ");
				
			}
			
			response.sendRedirect(CONTEXT + CERTIFICATE_CREATE_SERVLET);
			
		}else if(URL.equals(CERTIFICATE_EDIT_SERVLET)){
			int certificationId = Integer.parseInt(request.getParameter("certificationId"));
			if(isFieldNull){
				session.setAttribute("CertificationNullError", "Please fill up all the fields! ");
				
			}
			
			if(!isFieldNull){ 			
				certificate.setId(certificationId);
				certificate.setEmpId(empId);
				certificate.setTitle(title);
				certificate.setDateCert(date_cert);			 
				certificate.setVenue(venue);			 
				certificate.setDetails(details);
				certificationRepository.updateCertification(certificate);								
				session.setAttribute("SuccessCertificationUpdate", "Successfully updated! ");
		
			}  
			response.sendRedirect(CONTEXT + CERTIFICATE_EDIT_SERVLET + "?id=" + certificationId); 
		
		}
		
	} 
}
