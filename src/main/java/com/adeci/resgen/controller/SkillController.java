package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.CONTEXT;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_SERVLET;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adeci.resgen.domain.PersonalSkill;
import com.adeci.resgen.repository.SkillRepository;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Skill"})
public class SkillController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher; 
	private SkillRepository skillRepository = new SkillRepository();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String empId = (String) session.getAttribute("SessionEmployeeId");
		
		request.setAttribute("activeSkills", skillRepository.getAllActiveSkills());
		request.setAttribute("personalSkill", skillRepository.getUserSkillByEmployeeId(empId));
		dispatcher = request.getRequestDispatcher(SKILL_PAGE);
        dispatcher.forward(request, response);
        
	} 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CONTEXT = request.getContextPath();
		HttpSession session = request.getSession();
		
		String empId = (String) session.getAttribute("SessionEmployeeId"); 
		String[] skillIds = request.getParameterValues("skillId");  
		
		PersonalSkill personalSkill = new PersonalSkill();
		
		for(String  skillId : skillIds){
			String skillName = request.getParameter("skillName-"+skillId);
			String rating = request.getParameter("rating-"+skillId);
			
			if(skillName != null && rating !=null){
				int id = Integer.parseInt(skillId); 
				personalSkill.setSkillId(id);
				personalSkill.setEmployeeId(empId);
				personalSkill.setSkill(skillName);
				personalSkill.setActive("Active");
				personalSkill.setRating(rating);
				skillRepository.updatePersonalSkill(personalSkill);
				
			}
		}
		 
		response.sendRedirect(CONTEXT + SKILL_SERVLET); 
		
	} 
}
