package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.ASSETS_IMAGE_DIRECTORY;
import static com.adeci.resgen.controller.util.VariableConstants.CONTEXT;
import static com.adeci.resgen.controller.util.VariableConstants.PERSONAL_IMAGE_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.PERSONAL_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.PERSONAL_SERVLET;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.adeci.resgen.domain.Personal;
import com.adeci.resgen.repository.PersonalRepository;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Personal",
							"/Personal/Profile-image"})
public class PersonalController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher;
	private PersonalRepository personalRepository = new PersonalRepository();
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String empId = (String) session.getAttribute("SessionEmployeeId");
		
		request.setAttribute("personalInfo", personalRepository.getPersonalByEmployeeId(empId));
		
		dispatcher = request.getRequestDispatcher(PERSONAL_PAGE);
        dispatcher.forward(request, response);
	} 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CONTEXT = request.getContextPath();
		Personal personal = new Personal();
		String URL = request.getServletPath();
		
		if(URL.equals(PERSONAL_SERVLET)){
			personal.setLastName(request.getParameter("lastName"));
			personal.setFirstName(request.getParameter("firstName"));
			personal.setMiddleName(request.getParameter("middleName"));
			personal.setAddress(request.getParameter("address"));
			personal.setContact(request.getParameter("contact"));
			personal.setEmail(request.getParameter("email"));
			personal.setBirthDate(request.getParameter("birthday"));
			personal.setUserRole(request.getParameter("role"));
			personal.setLevel(request.getParameter("level"));
			personal.setCivilStatus(request.getParameter("civilStatus"));
			personal.setOverView(request.getParameter("overview"));
			personal.setEmployeeId(request.getParameter("empId"));
			personalRepository.updatePersonal(personal); 
			response.sendRedirect(CONTEXT + PERSONAL_SERVLET); 
			
		} else if (URL.equals(PERSONAL_IMAGE_SERVLET)){ 
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                // Create a factory for disk-based file items
                FileItemFactory factory = new DiskFileItemFactory();
                // Create a new file upload handler
                ServletFileUpload upload = new ServletFileUpload(factory);

                try {
                    HttpSession session = request.getSession();
                    List items = upload.parseRequest(request);
                    Iterator iterator = items.iterator();
                    
                    while (iterator.hasNext()) {
                        FileItem item = (FileItem) iterator.next();
                        
                        if (!item.isFormField()) {
                            String fileName = item.getName();    
                            String root = getServletContext().getRealPath("/");  
                            System.out.println(root);
                            File path = new File(root + ASSETS_IMAGE_DIRECTORY);
                            
                            if (!path.exists()) {
                                boolean status = path.mkdirs();
                            }

                            File uploadedFile = new File(path + "/" + fileName); 
                            personal.setEmployeeId((String) session.getAttribute("SessionEmployeeId"));
                            personal.setPicture(fileName);
                            personalRepository.updatePersonalPicture(personal);
                            item.write(uploadedFile);
                        }
                    }
                } catch (FileUploadException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            response.sendRedirect(CONTEXT + PERSONAL_SERVLET);
		}
	} 
}
