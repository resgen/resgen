package com.adeci.resgen.controller.util;

public class VariableConstants {
	private VariableConstants(){}
	
	public static String CONTEXT; 
	
	//WEB PAGE
	public static final String LOGIN_PAGE = "/WEB-INF/main/Login.jsp";
	public static final String PERSONAL_PAGE = "/WEB-INF/main/EmployeeProfile/personal.jsp";
	public static final String EXPERIENCE_PAGE = "/WEB-INF/main/EmployeeProfile/experience/experience.jsp";
	public static final String EXPERIENCE_ADD_PAGE = "/WEB-INF/main/EmployeeProfile/experience/experienceCreate.jsp";
	public static final String EXPERIENCE_PROJECT_PAGE = "/WEB-INF/main/EmployeeProfile/experience/experienceProjects.jsp";
	public static final String EXPERIENCE_PROJECT_ADD_PAGE = "/WEB-INF/main/EmployeeProfile/experience/experienceProjectsCreate.jsp";
	public static final String EXPERIENCE_PROJECT_EDIT_PAGE = "/WEB-INF/main/EmployeeProfile/experience/experienceProjectsEdit.jsp";
	public static final String USER_MAINTENANCE_PAGE = "/WEB-INF/main/Maintenance/userMaintenance.jsp";
	public static final String USER_MAINTENANCE_EDIT_PAGE = "/WEB-INF/main/Maintenance/userMaintenanceEdit.jsp";
	public static final String USER_MAINTENANCE_ADD_PAGE = "/WEB-INF/main/Maintenance/userMaintenanceAdd.jsp";
	public static final String SKILL_MAINTENANCE_PAGE = "/WEB-INF/main/Maintenance/skillMaintenance.jsp";
	public static final String SKILL_MAINTENANCE_EDIT_PAGE = "/WEB-INF/main/Maintenance/skillMaintenanceEdit.jsp";
	public static final String SKILL_MAINTENANCE_ADD_PAGE = "/WEB-INF/main/Maintenance/skillMaintenanceAdd.jsp";
	public static final String HOME_PAGE = "/WEB-INF/main/index.jsp";
	public static final String CHANGE_PASSWORD_PAGE = "/WEB-INF/main/ChangePassword.jsp";
	public static final String REGISTER_PAGE = "/WEB-INF/main/Register.jsp"; 
	public static final String CERTIFICATION_PAGE = "/WEB-INF/main/EmployeeProfile/certification/certification.jsp";
	public static final String CERTIFICATION_ADD_PAGE = "/WEB-INF/main/EmployeeProfile/certification/certificationCreate.jsp";
	public static final String CERTIFICATION_EDIT_PAGE = "/WEB-INF/main/EmployeeProfile/certification/certificationEdit.jsp";
	public static final String EDUCATION_PAGE = "/WEB-INF/main/EmployeeProfile/education/education.jsp";
	public static final String EDUCATION_ADD_PAGE = "/WEB-INF/main/EmployeeProfile/education/educationCreate.jsp";
	public static final String EDUCATION_EDIT_PAGE = "/WEB-INF/main/EmployeeProfile/education/educationEdit.jsp";
	public static final String SKILL_PAGE = "/WEB-INF/main/EmployeeProfile/skill/skill.jsp";
	
	//SERVLETS
	public static final String ERROR_404_SERVLET = "/404";
	public static final String ASSETS = "/assets";
	public static final String SETTING_CHANGE_PASSWORD = "/Change-password";
	public static final String ASSETS_IMAGE_DIRECTORY = "/assets/image";
	public static final String REGISTER_SERVLET = "/Register";
	public static final String LOGIN_SERVLET = "/Login"; 
	public static final String LOGIN_PROCESS_SERVLET = "/Login-process";
	public static final String LOGOUT_SERVLET = "/Logout";
	public static final String HOME_SERVLET = "/Home";
	public static final String PERSONAL_SERVLET = "/Personal";
	public static final String PERSONAL_IMAGE_SERVLET = "/Personal/Profile-image";
	public static final String EXPERIENCE_SERVLET = "/Experience";
	public static final String EXPERIENCE_CREATE_SERVLET = "/Experience/Create";
	public static final String EXPERIENCE_DELETE_SERVLET = "/Experience/Delete";
	public static final String EXPERIENCE_COMPANY_SERVLET = "/Experience/Company";
	public static final String EXPERIENCE_COMPANY_CREATE_SERVLET = "/Experience/Company/Create";
	public static final String EXPERIENCE_COMPANY_PROJECT_SERVLET = "/Experience/Company/Project";
	public static final String EXPERIENCE_COMPANY_PROJECT_EDIT_SERVLET = "/Experience/Company/Project/Edit";
	public static final String EXPERIENCE_COMPANY_PROJECT_DELETE_SERVLET = "/Experience/Company/Project/Delete";
	public static final String CERTIFICATE_SERVLET = "/Certification";
	public static final String CERTIFICATE_CREATE_SERVLET = "/Certification/Create";
	public static final String CERTIFICATE_DELETE_SERVLET = "/Certification/Delete";
	public static final String CERTIFICATE_EDIT_SERVLET = "/Certification/Edit";
	public static final String EDUCATION_SERVLET = "/Education";
	public static final String EDUCATION_CREATE_SERVLET = "/Education/Create";
	public static final String EDUCATION_DELETE_SERVLET = "/Education/Delete";
	public static final String EDUCATION_EDIT_SERVLET = "/Education/Edit";
	public static final String SKILL_SERVLET = "/Skill";
	
	//ADMIN SERVLETS
	public static final String SKILL_MAINTENANCE_SERVLET = "/SkillMaintenance";
	public static final String SKILL_MAINTENANCE_EDIT_SERVLET = "/SkillMaintenance/Skill";
	public static final String SKILL_MAINTENANCE_CREATE_SERVLET = "/SkillMaintenance/Create";
	public static final String SKILL_MAINTENANCE_STATUS_SERVLET = "/SkillMaintenance/Skill/Status";
	public static final String USER_MAINTENANCE_SERVLET = "/UserMaintenance";
	public static final String USER_MAINTENANCE_EDIT_SERVLET = "/UserMaintenance/User";
	public static final String USER_MAINTENANCE_CREATE_SERVLET = "/UserMaintenance/Create";
	public static final String USER_MAINTENANCE_STATUS_SERVLET = "/UserMaintenance/User/Status";
	
	
	
	
}


