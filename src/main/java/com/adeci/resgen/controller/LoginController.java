package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.CONTEXT;
import static com.adeci.resgen.controller.util.VariableConstants.HOME_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.LOGIN_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.LOGIN_SERVLET;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adeci.resgen.domain.PersonalAndUser;
import com.adeci.resgen.repository.UserRepository;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Login","/Login-process","/Logout"})
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher; 
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String URL = request.getServletPath();
		CONTEXT = request.getContextPath();
		
        if(URL.equals(LOGIN_SERVLET)){
        	// Error Validation
        	if (session.getAttribute("LoginError") != null){
        		request.setAttribute("LoginError", session.getAttribute("LoginError"));
        		session.removeAttribute("LoginError");
        	}
        	
    		dispatcher = request.getRequestDispatcher(LOGIN_PAGE);
            dispatcher.forward(request, response); 
            
        } else {
        	session.invalidate();
            response.sendRedirect(CONTEXT + LOGIN_SERVLET);
            
        }
	} 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		 String employeeId = request.getParameter("employeeId");
		 String passWord = request.getParameter("passWord");
		 
		 HttpSession session = request.getSession(); 
		 CONTEXT = request.getContextPath();
		 UserRepository userRepository = new UserRepository();
		 List<PersonalAndUser> userDetails = new ArrayList<PersonalAndUser>();
		 userDetails = userRepository.getUserByEmployeeId(employeeId);
		 boolean isUserActive = userRepository.isUserEmployeeActive(employeeId);
		 boolean isUserAuthenticated = userRepository.getUserAuthentication(employeeId, passWord);
		 
		 if(isUserAuthenticated && isUserActive){
			 session.setAttribute("SessionEmployeeId" , employeeId);
			 for(PersonalAndUser user : userDetails){ 
				 session.setAttribute("SessionUserType", user.getUserType());
			 }
			 response.sendRedirect(CONTEXT + HOME_SERVLET);
			 
		 } else {
			 // Set Error Validation
			 session.setAttribute("LoginError", "Invalid Credentials / Inactive User!");
			 response.sendRedirect(CONTEXT + LOGIN_SERVLET);
			 
		 }
		 
	}

}
