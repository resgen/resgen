package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.CONTEXT;
import static com.adeci.resgen.controller.util.VariableConstants.ERROR_404_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_MAINTENANCE_ADD_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_MAINTENANCE_CREATE_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_MAINTENANCE_EDIT_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_MAINTENANCE_EDIT_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_MAINTENANCE_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_MAINTENANCE_SERVLET;
import static com.adeci.resgen.controller.util.VariableConstants.SKILL_MAINTENANCE_STATUS_SERVLET;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.adeci.resgen.domain.Skill;
import com.adeci.resgen.repository.SkillRepository;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/SkillMaintenance",
							"/SkillMaintenance/Skill",
							"/SkillMaintenance/Create",
							"/SkillMaintenance/Skill/Status"})
public class SkillMaintenanceController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher;
	private SkillRepository skillRepository = new SkillRepository();
	private Skill skill = new Skill();
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String URL = request.getServletPath();
		CONTEXT = request.getContextPath(); 
		
		if(URL.equals(SKILL_MAINTENANCE_SERVLET)){
			String search = request.getParameter("skillSearchInput"); 
			
			if(search == null){
				request.setAttribute("skills", skillRepository.getAllSkills()); 
				
			} else {
				request.setAttribute("skills", skillRepository.getSkillMaintenanceBySkill(search));
				
			} 
			
			//Remove Search parameter in URL
			if(search == ""){
				response.sendRedirect(CONTEXT + SKILL_MAINTENANCE_SERVLET);
				
			} else {
				dispatcher = request.getRequestDispatcher(SKILL_MAINTENANCE_PAGE);
		        dispatcher.forward(request, response);	
		        
			}
	        
		} else if(URL.equals(SKILL_MAINTENANCE_CREATE_SERVLET)) {
			dispatcher = request.getRequestDispatcher(SKILL_MAINTENANCE_ADD_PAGE);
	        dispatcher.forward(request, response);	
	        
		} else if(URL.equals(SKILL_MAINTENANCE_STATUS_SERVLET)){ 
			int userId = Integer.parseInt(request.getParameter("id"));
			String status = request.getParameter("status").equals("Active") ? "Inactive" : "Active";
			  
			skillRepository.updateSkillMaintenanceStatus(userId,status);
			response.sendRedirect(CONTEXT + SKILL_MAINTENANCE_SERVLET); 

		} else if(URL.equals(SKILL_MAINTENANCE_EDIT_SERVLET)){ 
			int id = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("skillInfo", skillRepository.getSkillMaintenanceById(id));
			boolean isSkillActive = skillRepository.isSkillMaintenanceIdActive(id);
			
			if(isSkillActive){ 
				dispatcher = request.getRequestDispatcher(SKILL_MAINTENANCE_EDIT_PAGE);
		        dispatcher.forward(request, response);	
		        
			} else {
				response.sendRedirect(CONTEXT + ERROR_404_SERVLET);
				
			}
	        
		} else {
			response.sendRedirect(CONTEXT + SKILL_MAINTENANCE_SERVLET);
		}
	} 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CONTEXT = request.getContextPath(); 
		String URL = request.getServletPath(); 
		
		if(URL.equals(SKILL_MAINTENANCE_EDIT_SERVLET)){ 
			String status = request.getParameter("skillStatus") == null ? "Inactive" : "Active";
			int userId = Integer.parseInt(request.getParameter("skillId"));
			skill.setId(userId);
			skill.setSkill(request.getParameter("skillName").toUpperCase()); 
			skill.setActive(status);
			skillRepository.updateSkill(skill);
			response.sendRedirect(status == "Active" ? CONTEXT + SKILL_MAINTENANCE_EDIT_SERVLET+"?id="+userId : CONTEXT + SKILL_MAINTENANCE_SERVLET);
			
		} else if(URL.equals(SKILL_MAINTENANCE_CREATE_SERVLET)){ 
			skill.setSkill(request.getParameter("skillName")); 
			skill.setActive("Inactive");
			skillRepository.addSkill(skill);
			response.sendRedirect(CONTEXT + SKILL_MAINTENANCE_CREATE_SERVLET);
			
		} else {
			response.sendRedirect(CONTEXT + SKILL_MAINTENANCE_SERVLET);
			
		}
	}

}
