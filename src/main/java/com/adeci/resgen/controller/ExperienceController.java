package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adeci.resgen.domain.Experience;
import com.adeci.resgen.domain.UserProject;
import com.adeci.resgen.repository.ExperienceRepository;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Experience",
							"/Experience/Create",
							"/Experience/Company",
							"/Experience/Delete",
							"/Experience/Company/Create",
							"/Experience/Company/Project",
							"/Experience/Company/Project/Edit",
							"/Experience/Company/Project/Delete"})
public class ExperienceController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher;
	private ExperienceRepository experienceRepository = new ExperienceRepository();
	private Experience experience = new Experience(); 
	private UserProject userProject = new UserProject();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String empId =  (String) session.getAttribute("SessionEmployeeId");
		String URL = request.getServletPath();
		CONTEXT = request.getContextPath(); 
		
		if(URL.equals(EXPERIENCE_SERVLET)){ 
			request.setAttribute("experienceInfo", experienceRepository.getExperienceByEmployeeId(empId));
			dispatcher = request.getRequestDispatcher(EXPERIENCE_PAGE);
	        dispatcher.forward(request, response);	
	        
		} else if(URL.equals(EXPERIENCE_DELETE_SERVLET)){
			experience.setEmployeeId(empId);
			experience.setId(Integer.parseInt(request.getParameter("id")));
			experienceRepository.deleteExperience(experience);
			response.sendRedirect(CONTEXT + EXPERIENCE_SERVLET);
			
		} else if(URL.equals(EXPERIENCE_COMPANY_SERVLET)){
			int companyId = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("companyInfo", experienceRepository.getExperienceByCompanyId(companyId,empId));
			request.setAttribute("projectsInfo", experienceRepository.getProjectByCompanyId(companyId,empId));
			dispatcher = request.getRequestDispatcher(EXPERIENCE_PROJECT_PAGE);
	        dispatcher.forward(request, response);
	        
		} else if(URL.equals(EXPERIENCE_CREATE_SERVLET)){
			dispatcher = request.getRequestDispatcher(EXPERIENCE_ADD_PAGE);
	        dispatcher.forward(request, response);
	        
		} else if(URL.equals(EXPERIENCE_COMPANY_CREATE_SERVLET)){
			int companyId = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("companyInfo", experienceRepository.getExperienceByCompanyId(companyId,empId));
			dispatcher = request.getRequestDispatcher(EXPERIENCE_PROJECT_ADD_PAGE);
	        dispatcher.forward(request, response);
	        
		} else if(URL.equals(EXPERIENCE_COMPANY_PROJECT_SERVLET)){
			int projectId = Integer.parseInt(request.getParameter("id")); 
			request.setAttribute("projectInfo", experienceRepository.getProjectById(projectId, empId)); 
			dispatcher = request.getRequestDispatcher(EXPERIENCE_PROJECT_EDIT_PAGE);
	        dispatcher.forward(request, response);
	        
		} else if(URL.equals(EXPERIENCE_COMPANY_PROJECT_DELETE_SERVLET)){
			int companyId = Integer.parseInt(request.getParameter("company"));
			experienceRepository.deleteProject(Integer.parseInt(request.getParameter("id")));
			response.sendRedirect(CONTEXT + EXPERIENCE_COMPANY_SERVLET + "?id=" + companyId);
			
		} else {
			response.sendRedirect(CONTEXT + EXPERIENCE_SERVLET);
			
		}
		
	} 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String empId =  (String) session.getAttribute("SessionEmployeeId");
		String URL = request.getServletPath();
		CONTEXT = request.getContextPath(); 
		
		if(URL.equals(EXPERIENCE_CREATE_SERVLET)){
			experience.setEmployeeId(empId); 
			experience.setStartDate(request.getParameter("startDate"));
			experience.setEndDate(request.getParameter("endDate"));
			experience.setCompanyName(request.getParameter("companyName"));
			experience.setPosition(request.getParameter("position"));
			experienceRepository.addExperience(experience);
			response.sendRedirect(CONTEXT + EXPERIENCE_SERVLET); 
			
		} else if(URL.equals(EXPERIENCE_COMPANY_CREATE_SERVLET)){ 
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			userProject.setProjectName(request.getParameter("projectName"));
			userProject.setDescription(request.getParameter("description"));
			userProject.setRole(request.getParameter("role"));
			userProject.setTechnologies(request.getParameter("technologies")); 
			userProject.setCompanyName(request.getParameter("companyName"));
			userProject.setCompanyId(companyId);
			userProject.setEmployeeId(request.getParameter("employeeId"));
			experienceRepository.addExperienceProject(userProject);
			response.sendRedirect(CONTEXT + EXPERIENCE_COMPANY_SERVLET + "?id=" + companyId);
			
		} else if(URL.equals(EXPERIENCE_COMPANY_PROJECT_EDIT_SERVLET)){
			int companyId = Integer.parseInt(request.getParameter("companyId"));
			int projectId = Integer.parseInt(request.getParameter("projectId"));
			userProject.setEmployeeId(empId);
			userProject.setId(projectId);
			userProject.setProjectName(request.getParameter("projectName"));
			userProject.setDescription(request.getParameter("description"));
			userProject.setRole(request.getParameter("role"));
			userProject.setTechnologies(request.getParameter("technologies"));
			userProject.setCompanyId(companyId);
			userProject.setCompanyName(request.getParameter("companyName"));
			experienceRepository.updateProject(userProject);
			response.sendRedirect(CONTEXT + EXPERIENCE_COMPANY_PROJECT_SERVLET + "?id=" + projectId);
			
		} else {
			response.sendRedirect(CONTEXT + EXPERIENCE_SERVLET);
			
		}
	} 
}
