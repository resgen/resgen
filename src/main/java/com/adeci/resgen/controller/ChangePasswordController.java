package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adeci.resgen.repository.UserRepository;
import com.adeci.resgen.security.algorithm.Encryption;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/Change-password"})
public class ChangePasswordController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher; 
	private UserRepository userRepository = new UserRepository();
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		//Check for errors
		if(session.getAttribute("PasswordNullError") != null){
			request.setAttribute("PasswordNullError", session.getAttribute("PasswordNullError"));
			session.removeAttribute("PasswordNullError");
			
		}
		
		if(session.getAttribute("CurrentPasswordError") != null){
			request.setAttribute("CurrentPasswordError", session.getAttribute("CurrentPasswordError"));
			session.removeAttribute("CurrentPasswordError");
			
		}
		
		if(session.getAttribute("ConfirmPasswordError") != null){
			request.setAttribute("ConfirmPasswordError", session.getAttribute("ConfirmPasswordError"));
			session.removeAttribute("ConfirmPasswordError");
			
		}
		
		if(session.getAttribute("PasswordChange") != null){
			request.setAttribute("PasswordChange", session.getAttribute("PasswordChange"));
			session.removeAttribute("PasswordChange");
			
		}
		
		dispatcher = request.getRequestDispatcher(CHANGE_PASSWORD_PAGE);
        dispatcher.forward(request, response);
        
	} 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		CONTEXT = request.getContextPath();
		Encryption encryption = new Encryption();
		
		String empId = (String) session.getAttribute("SessionEmployeeId");
		String newPassword = request.getParameter("newPassword");
		String confirmPassword = request.getParameter("confirmPassword");
		String currentPassword = request.getParameter("currentPassword");
		boolean isFieldNull = newPassword == null || newPassword == "" || confirmPassword == null
								|| confirmPassword == "" || currentPassword == null || currentPassword == "";
		boolean isCurrentPasswordCorrect = userRepository.getUserAuthentication(empId, currentPassword);
		boolean isConfirmPasswordCorrect = newPassword.equals(confirmPassword);
		
		// Error Validation
		if(isFieldNull){
			session.setAttribute("PasswordNullError", "Please fill up all the fields! ");

		}
		
		if(!isFieldNull && !isCurrentPasswordCorrect){
			session.setAttribute("CurrentPasswordError", " Current password incorrect! ");

		}
		
		if(!isFieldNull && !isConfirmPasswordCorrect){
			session.setAttribute("ConfirmPasswordError", " Confirm password incorrect! ");

		}
		
		if(!isFieldNull && isCurrentPasswordCorrect && isConfirmPasswordCorrect){
			userRepository.updateUserPassword(empId, encryption.encryptPassword(newPassword));
			session.setAttribute("PasswordChange", " Password changed successfully! ");

		}

		response.sendRedirect(CONTEXT + SETTING_CHANGE_PASSWORD);
		
	}

}
