package com.adeci.resgen.controller;

import static com.adeci.resgen.controller.util.VariableConstants.CONTEXT;
import static com.adeci.resgen.controller.util.VariableConstants.REGISTER_PAGE;
import static com.adeci.resgen.controller.util.VariableConstants.REGISTER_SERVLET;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adeci.resgen.domain.Personal;
import com.adeci.resgen.domain.User;
import com.adeci.resgen.repository.PersonalRepository;
import com.adeci.resgen.repository.UserRepository;
import com.adeci.resgen.security.algorithm.Encryption;
import com.adeci.resgen.utility.regex.EmailRegex;

@WebServlet("/Register")
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static RequestDispatcher dispatcher; 
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(); 
		
		//Check for errors
		if(session.getAttribute("RegisterDuplicateError") != null){
			request.setAttribute("RegisterDuplicateError", session.getAttribute("RegisterDuplicateError"));
			session.removeAttribute("RegisterDuplicateError");
			
		}
		
		if(session.getAttribute("RegisterNullError") != null){
			request.setAttribute("RegisterNullError", session.getAttribute("RegisterNullError"));
			session.removeAttribute("RegisterNullError");
			
		}
		
		if(session.getAttribute("SuccessRegister") != null){
			request.setAttribute("SuccessRegister", session.getAttribute("SuccessRegister"));
			session.removeAttribute("SuccessRegister");
			
		} 
		
		if(session.getAttribute("RegisterEmailError") != null){
			request.setAttribute("RegisterEmailError", session.getAttribute("RegisterEmailError"));
			session.removeAttribute("RegisterEmailError");
			
		}
		
		dispatcher = request.getRequestDispatcher(REGISTER_PAGE);
        dispatcher.forward(request, response);
	}
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(); 
		CONTEXT = request.getContextPath(); 
		 
		EmailRegex emailRegex = new EmailRegex();
		User user = new User();
		Personal personal = new Personal();
		Encryption encryption = new Encryption();
		UserRepository userRepository = new UserRepository();
		PersonalRepository personalRepository = new PersonalRepository();
		
		String employeeId = request.getParameter("employeeId"); 
		String email = request.getParameter("email");
		String firstName = request.getParameter("firstName");
		String middleName = request.getParameter("middleName");
		String lastName = request.getParameter("lastName");
		boolean isDuplicateUser = userRepository.isDuplicateEmployeeId(employeeId);
		boolean isFieldNull = employeeId == null ||  email == null || firstName == null || middleName == null || lastName == null ||
								employeeId == "" ||  email == "" || firstName == "" || middleName == "" || lastName == "";
		boolean isValidEmail = emailRegex.checkEmailPattern(email);
		
		//Error Validation
		if(isDuplicateUser){ 
			session.setAttribute("RegisterDuplicateError", "Duplicate employee id!");
		}
		
		if(isFieldNull){
			session.setAttribute("RegisterNullError", "Please fill up all the fields! ");
		}  
		
		if(!isValidEmail){
			session.setAttribute("RegisterEmailError", "Invalid email!");
		}
		
		if(!isDuplicateUser && !isFieldNull && isValidEmail) {
			//Encrypt Passaword = Default pass word Uppercase Lastname
			user.setPassWord(encryption.encryptPassword(lastName.toUpperCase()));
			user.setEmployeeId(employeeId);
			user.setActive("Inactive");
			user.setUserType("Employee");
			user.setEmail(email); 
			personal.setFirstName(firstName);
			personal.setMiddleName(middleName);
			personal.setLastName(lastName);
			personal.setEmail(email);
			personal.setEmployeeId(employeeId); 
			
			userRepository.addUser(user);
			personalRepository.addPersonal(personal); 
			session.setAttribute("SuccessRegister", " You have successfully registered! ");
			
		}
		response.sendRedirect(CONTEXT + REGISTER_SERVLET); 
		
	} 
}
